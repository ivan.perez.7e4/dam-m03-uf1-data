package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class FirstBigger {

        public static void main(String[] args) {

            //poner 2 valores

            Scanner scanner = new Scanner(System.in);
            int valor1 = scanner.nextInt();
            int valor2 = scanner.nextInt();

            //true si es mas grande y si no false

            boolean firstbigger = valor1 > valor2;

            //print result
            System.out.println(firstbigger);

        }
}
