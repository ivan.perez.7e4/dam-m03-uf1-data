package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class CalculateDiscount {
    public static void main(String[] args) {
        // leer 2  valores
        Scanner scanner = new Scanner(System.in);
        double valor1 = scanner.nextDouble();
        double valor2 = scanner.nextDouble();

        //restar
        double res1 = valor2 - valor1;

        //dividir por 100
        double result1 = res1 / valor2;

        //multiplicar por 100

        double result = result1 * 100;

        //resultado en porcentaje
        System.out.println(result + "%");

    }
}
