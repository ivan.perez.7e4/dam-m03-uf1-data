package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsValidNote {

    public static void main(String[] args) {

        //poner un valor

        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();

        //darle valor a los "billetes"

        boolean Note5 = valor1 == 5;
        boolean Note10 = valor1 == 10;
        boolean Note20 = valor1 == 20;
        boolean Note50 = valor1 == 50;
        boolean Note100 = valor1 == 100;
        boolean Note200 = valor1 == 200;
        boolean Note500 = valor1 == 500;

        // si coincide con billete true si no false

        boolean IsValidNote = Note5 || Note10 || Note20 || Note50 || Note100 || Note200 || Note500;

        //print result
        System.out.println(IsValidNote);

    }

}
