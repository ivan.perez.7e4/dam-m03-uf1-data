package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class DoubleDoubleMe {


    public static void main(String[] args) {
        // leer valor
        Scanner scanner = new Scanner(System.in);

        // multiplicar por dos el valor
        double userInputValue = scanner.nextDouble();
        double result = userInputValue * 2;

        // resultado

        System.out.println(result);

    }
}
