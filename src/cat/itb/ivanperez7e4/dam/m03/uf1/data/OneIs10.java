package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class OneIs10 {

    public static void main(String[] args) {

        //poner 4 valores

        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();
        int valor2 = scanner.nextInt();
        int valor3 = scanner.nextInt();
        int valor4 = scanner.nextInt();

        //si 1 de los 4 valores es 10 true, si no false

        boolean result1 = valor1 == 10;
        boolean result2 = valor2 == 10;
        boolean result3 = valor3 == 10;
        boolean result4 = valor4 == 10;

        boolean finalresult = result1 || result2 || result3 || result4;


        System.out.println(finalresult);


    }
}
