package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class HelloWorld {
    /**
     *  Prints a welcome message
     */
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int userInputValue = scanner.nextInt();
        System.out.println(userInputValue);


    }

}
