package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class HowBigIsMyPizza {


    public static void main(String[] args) {

        // Area = Π · r2


        // leer valor
        Scanner scanner = new Scanner(System.in);
        double valor1 = scanner.nextDouble();

        //Diametro / 2 igual a radio

        double radio = valor1 / 2;

        // radio elevado a 2 por numero pi

        double result = Math.PI * Math.pow (radio,2);

        //resultado

        System.out.println(result);
    }
}
