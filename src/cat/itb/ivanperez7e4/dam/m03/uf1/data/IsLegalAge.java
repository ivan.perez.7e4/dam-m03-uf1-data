package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class IsLegalAge {

    public static void main(String[] args) {

        //ask age to user

        Scanner scanner = new Scanner (System.in);
        int age = scanner.nextInt();


        //calculate legal age (age >=18)
        boolean islegalage = age >=18;

        //print result
        System.out.println(islegalage);

    }

}
