package cat.itb.ivanperez7e4.dam.m03.uf1.data;

import java.util.Scanner;

public class NiceIsLegalAge {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Escribe tu edad");
        int edat = scanner.nextInt();

        if (edat >= 18){
            System.out.println("Eres mayor de edad");
        }

    }
}
