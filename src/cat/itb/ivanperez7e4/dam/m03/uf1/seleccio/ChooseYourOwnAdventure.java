package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class ChooseYourOwnAdventure {

    public static void main(String[] args) {
        System.out.println("Et despertes a una illa de nit. L'últim que recordes és ofegar-se.");
        System.out.println("Que vols fer?");
        System.out.println("1. Espero");
        System.out.println("2. Vaig a cercar menjar");
        Scanner scanner = new Scanner(System.in);
        int option = scanner.nextInt();
        if (option == 1)
            dead();
        else
            dia();

    }

    private static void dia() {
        System.out.println("S'ha fet de dia. Tens molta gana");
        System.out.println("Que vols fer?");
        System.out.println("1. Espero");
        System.out.println("2. Vaig a cercar menjar");
        Scanner scanner1 = new Scanner(System.in);
        int option1 = scanner1.nextInt();
        if(option1==1)
            dead();
        else
            coco();

    }

    private static void salvado() {
        System.out.println("Desde dalt de la palmera veus un vaixell. Estàs salvat!");
    }

    private static void coco() {
        System.out.println("Hi ha una palmera amb cocos");
        System.out.println("Que vols fer?");
        System.out.println("1. No hi pujo, és perillós");
        System.out.println("2. Hi pujo");
        Scanner scanner2 = new Scanner(System.in);
        int option2 = scanner2.nextInt();
        if (option2 == 1)
            dead();
        else
            salvado();
    }

    private static void dead() {
        System.out.println("Has mort");

    }
}
