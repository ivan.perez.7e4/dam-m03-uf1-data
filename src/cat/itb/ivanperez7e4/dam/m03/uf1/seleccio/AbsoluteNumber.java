package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class AbsoluteNumber {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int valor1 = scanner.nextInt();

        if (valor1 < 0) {
            System.out.println(-valor1);

        }
        else {
            System.out.println(valor1);

        }




    }
}
