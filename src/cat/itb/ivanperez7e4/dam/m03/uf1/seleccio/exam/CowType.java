package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;

public class CowType {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Posa l'edat de l'animal");
        int edat = scanner.nextInt();

        if (edat<=2)
            System.out.println("vedell");
        else
            adults();
    }

    private static void adults() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Posa el sexe (1 = mascle, 2 = femella)");
        int sexe = scanner.nextInt();
        if (sexe==2)
            System.out.println("Vaca");
        else
            capat1();
    }

    private static void capat1() {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Posa si esta capat (1 = no capat, 2 = capat)");
        int capat = scanner.nextInt();

        if (capat==1)
            System.out.println("Toro");
        else if (capat==2)
            System.out.println("Bou");

    }
}
