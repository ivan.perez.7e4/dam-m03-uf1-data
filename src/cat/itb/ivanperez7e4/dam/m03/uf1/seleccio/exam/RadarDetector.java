package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio.exam;

import java.util.Scanner;

public class RadarDetector {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Posa una velocitat");
        double velocidad = scanner.nextDouble();

        if (velocidad >= 140){
            System.out.println("Multa greu");
        }
        else if (velocidad>120 && velocidad<140){
            System.out.println("Multa lleu");
        }
        else if (velocidad<=120){
            System.out.println("Correcte");
        }

    }
}
