package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IsLeapYear {

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

            int año = scanner.nextInt();

            //Es divisible entre 4 y no es divisible entre 100
            //Es divisible entre 100 y 400

            if (año % 4 == 0 && año % 100 != 0) {

                System.out.println(año + "és any de traspàs");
            } else if (año % 100 == 0 && año % 400 == 0) {

                System.out.println(año + " és any de traspàs");
            } else {
                System.out.println(año + " no és any de traspàs");
            }

        }
    }

