package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class RockPaperScissors {
    public static void main(String[] args) {

        //L'usuari introdueix dos enters (1)pedra, (2) paper, (3) tisora.

        Scanner scanner = new Scanner(System.in);
        System.out.println("User1 escoge");
        System.out.println("1-Piedra");
        System.out.println("2-Papel");
        System.out.println("3-Tijeras");
        int user1 = scanner.nextInt();
        System.out.println("User2 escoge");
        System.out.println("1-Piedra");
        System.out.println("2-Papel");
        System.out.println("3-Tijeras");
        int user2 = scanner.nextInt();

        if (user1 == user2) {
            System.out.println("Empat");
        }
        else if (user1 == 1 && user2 == 3 || user1 == 2 && user2 == 1 || user1 == 3 && user2 == 2)  {
            System.out.println("Gana el primero");
        }
        else if (user2 == 1 && user1 == 3 || user2 == 2 && user1 == 1 || user2 == 3 && user1 == 2) {
            System.out.println("Gana el segundo");
        }

    }
}


