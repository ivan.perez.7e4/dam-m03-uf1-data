package cat.itb.ivanperez7e4.dam.m03.uf1.seleccio;

import java.util.Scanner;

public class IdentikitGenerator {

    // L'usuari ha d'introduïr el tipus de cabells, ulls, nas i boca i s'imprimeix per pantalla un dibuix de com és el sospitós.
    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        System.out.println("Escoge el cabello (rizados, lisos o peinados)");
        String cabello = scanner.nextLine();

        switch (cabello) {
            case "rizados":
                System.out.println("@@@@");
                break;
            case "lisos":
                System.out.println("VVVV");
                break;
            case "peinados":
                System.out.println("XXXX");
                break;
            default:
                System.out.println("No se que tipo de cabello es");

        }


        System.out.println("Escoge los ojos (cerrados, redondos o estrellados)");
        String ojos = scanner.nextLine();

        switch (ojos) {
            case "cerrados":
                System.out.println(".-.-.");
                break;
            case "redondos":
                System.out.println(".o-o.");
                break;
            case "estrellados":
                System.out.println("_.*-_.");
                break;
            default:
                System.out.println("No se que tipo de ojos son");

        }

        System.out.println("Escoge la nariz (aplastada, respingona o aguileña)");
                String nariz = scanner.nextLine();

        switch (nariz) {
            case "aplstada":
                System.out.println("..0..");
                break;
            case "respingona":
                System.out.println("..C..");
                break;
            case "aguileña":
                System.out.println("..V..");
                break;
            default:
                System.out.println("No se que tipo de nariz es");

        }
            System.out.println("Escoge la boca (normal, bigote o dientes_salidas)");
                        String boca = scanner.nextLine();

            switch (boca) {
                case "normal":
                    System.out.println(".===.");
                    break;
                case "bigote":
                    System.out.println(".∼∼∼.");
                    break;
                case "dientes_salidas":
                    System.out.println(".www.");
                    break;
                default:
                    System.out.println("No se que tipo de boca es");

            }
    }
}