package cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class RockPaperScissorsVsComputer {
    public static void main(String[] args) {
        System.out.println("Introduex un enter (1)pedra, (2) paper, (3) tisora");
        Scanner scanner = new Scanner(System.in);
        int user = scanner.nextInt();

        int pc = (int) (Math.random() * 3 + 1);

        switch (pc) {
            case 1:
                System.out.println("L'ordinador a tirat pedra");
                break;
            case 2:
                System.out.println("L'ordinador a tirat paper");
                break;
            case 3:
                System.out.println("L'ordinador a tirat tisores");
                break;
            default:
                System.out.println("ERROR");
        }
        if (user == pc) {
            System.out.println("Empat");
        } else if (user == 1 && pc == 3 || user == 2 && pc == 1 || user == 3 && pc == 2) {
            System.out.println("Has guanyat");
        } else if (pc == 1 && user == 3 || pc == 2 && user == 1 || pc == 3 && user == 2) {
            System.out.println("Guanya l'ordinador");

        }



    }
}