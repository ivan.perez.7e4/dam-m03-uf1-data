package cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IdentikitGeneratorFunc {

    // L'usuari ha d'introduïr el tipus de cabells, ulls, nas i boca i s'imprimeix per pantalla un dibuix de com és el sospitós.
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Escoge el cabello (rizados, lisos o peinados)");
        String cabello = scanner.nextLine();
        String hair = estiloCabello(cabello);

        System.out.println("Escoge los ojos (cerrados, redondos o estrellados)");
        String ojos = scanner.nextLine();
        String eyes = estilOjos(ojos);

        System.out.println("Escoge la nariz (aplastada, respingona o aguileña)");
        String nariz = scanner.nextLine();
        String nose = estiloNariz(nariz);

        System.out.println("Escoge la boca (normal, bigote o dientes_salidas)");
        String boca = scanner.nextLine();
        String mouth = estiloBoca(boca);

        System.out.println(hair);
        System.out.println(eyes);
        System.out.println(nose);
        System.out.println(mouth);
                }
        private static String estiloBoca(String boca) {
            switch (boca) {
                case "normal":
                    return ".===.";
                case "bigote":
                    return ".∼∼∼.";
                case "dientes_salidas":
                    return ".www.";
                default:
                   return "ERROR";
            }
        }
        private static String estiloNariz(String nariz) {
            switch (nariz) {
                case "aplastada":
                    return "..0..";
                case "respingona":
                    return "..C..";
                case "aguileña":
                    return "..V..";
                default:
                    return "ERROR";
            }
        }
        private static String estilOjos(String ojos) {
            switch (ojos) {
                case "cerrados":
                    return ".-.-.";
                case "redondos":
                    return ".o-o.";
                case "estrellados":
                    return  "_.*-*_.";
                default:
                   return "ERROR";
            }
        }
        private static String estiloCabello(String cabello) {
            switch (cabello) {
                case "rizados":
                    return "@@@@";
                case "lisos":
                    return "VVVV";
                case "peinados":
                    return "XXXX";
                default:
                    return  "ERROR";
            }
        }
    }