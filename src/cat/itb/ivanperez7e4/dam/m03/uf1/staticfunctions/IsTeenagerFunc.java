package cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class IsTeenagerFunc {


    public static void main(String[] args) {


        Scanner scanner = new Scanner(System.in);
        int edat = scanner.nextInt();


        boolean result = isTeenager(edat);


        System.out.println(result);

    }

    private static boolean isTeenager(int edat) {
        return edat > 10 && edat < 20;

    }
}
