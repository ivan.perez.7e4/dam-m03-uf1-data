package cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

import static cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions.HowBigIsMyPizzaFunc.areaPizza;
import static cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions.IntRectangleAreaFunc.rectangleArea;

public class WhichPizzaShouldIBuyFunc {

    public static void main(String[] args) {

        //diametro pizza redonda

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el diametro de la pizza");
        double valor1 = scanner.nextDouble();
        double superficiered = areaPizza(valor1);

        //dos costados pizza rectangular

        Scanner scanner1 = new Scanner(System.in);
        System.out.println("Introduce los costados de la pizza");
        double length = scanner.nextDouble();
        double width = scanner.nextDouble();

        double superficierec = rectangleArea(length, width);

        if (superficiered > superficierec){
            System.out.println("Compra la redonda");
        }
        else {
            System.out.println("Compra la rectangular");

        }
    }
}
