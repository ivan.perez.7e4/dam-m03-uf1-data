package cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class PowerOf {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el primer entero");
        int a = scanner.nextInt();
        System.out.println("Introduce el segundo entero");
        int b = scanner.nextInt();

        double result = Math.pow(a,b);

        System.out.println(result);

    }
}
