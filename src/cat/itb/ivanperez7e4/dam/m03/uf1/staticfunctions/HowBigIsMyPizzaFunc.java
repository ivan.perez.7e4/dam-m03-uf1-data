package cat.itb.ivanperez7e4.dam.m03.uf1.staticfunctions;

import java.util.Scanner;

public class HowBigIsMyPizzaFunc {


    public static void main(String[] args) {

        // Area = Π · r2

        Scanner scanner = new Scanner(System.in);
        double valor1 = scanner.nextDouble();


        double result = areaPizza(valor1);

        //resultado

        System.out.println(result);
    }

    public static double areaPizza(double valor1) {
        return Math.PI * Math.pow (valor1 / 2,2);
    }
}
