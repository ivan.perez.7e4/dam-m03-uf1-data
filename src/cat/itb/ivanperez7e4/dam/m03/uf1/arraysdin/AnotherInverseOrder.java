package cat.itb.ivanperez7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.Scanner;

public class AnotherInverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();
        ArrayList<Integer> list = new ArrayList<Integer>();
        while(enter!=-1) {
            list.add(0,enter);
            enter = scanner.nextInt();
        }
        System.out.println(list);
    }
}
