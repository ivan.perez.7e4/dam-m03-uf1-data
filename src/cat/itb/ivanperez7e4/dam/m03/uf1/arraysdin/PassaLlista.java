package cat.itb.ivanperez7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class PassaLlista {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();
        List<String> list = new ArrayList<String>(Arrays.asList("Magalí", "Magdalena", "Magí", "Manel", "Manela", "Manuel", "Manuela", "Mar", "Marc", "Margalida", "Marçal", "Marcel", "Maria", "Maricel", "Marina", "Marta", "Martí", "Martina"));

        while(enter!=-1){
            list.remove(enter);
            enter=scanner.nextInt();
        }
        System.out.println(list);
    }
}
