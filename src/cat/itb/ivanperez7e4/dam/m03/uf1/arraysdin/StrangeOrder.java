package cat.itb.ivanperez7e4.dam.m03.uf1.arraysdin;

import java.util.ArrayList;
import java.util.Scanner;

public class StrangeOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();
        ArrayList<Integer> list = new ArrayList<Integer>();
        boolean addToStart = true;
        while(enter!=-1) {
            if (addToStart) {
                list.add(0, enter);
            }
            else{
                list.add(enter);
            }
            addToStart=!addToStart;
            enter = scanner.nextInt();
        }
        System.out.println(list);
    }
}

