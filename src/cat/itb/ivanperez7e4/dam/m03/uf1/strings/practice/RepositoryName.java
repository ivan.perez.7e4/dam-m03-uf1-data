package cat.itb.ivanperez7e4.dam.m03.uf1.strings.practice;

import java.util.Scanner;

public class RepositoryName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int modulo = scanner.nextInt();
        int uf = scanner.nextInt();
        String email = scanner.next();
        String username = email.split("@")[0];
        System.out.printf("%s-dam-m%02d-uf%d",username.replaceAll("\\.",""),modulo,uf);
    }
}

