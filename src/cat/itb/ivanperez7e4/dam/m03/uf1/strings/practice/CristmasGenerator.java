package cat.itb.ivanperez7e4.dam.m03.uf1.strings.practice;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CristmasGenerator {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Introduce el año original y el año por el que lo cambiaremos");
        String Date= scanner.nextLine();
        String newDate= scanner.nextLine();
        System.out.println("Introduce el nombre original y el nombre por el que lo cambiaremos");
        String Name = scanner.nextLine();
        String newName = scanner.nextLine();
        System.out.println("Introduce el nombre original del autor y el nombre por el que lo cambiaremos");
        String AuthorName = scanner.nextLine();
        String newAuthorName = scanner.nextLine();

        List<String> txt = new ArrayList<>();
        String mensaje = scanner.nextLine();
        while(!mensaje.equals("END")){
            txt.add(mensaje);
            mensaje = scanner.nextLine();
        }
        for (int i = 0; 1<txt.size();i++) {
            System.out.printf("%s\n",(txt.get(i).replace(Date,newDate).replace(Name,newName).replace(AuthorName,newAuthorName)));
        }
    }
}


