package cat.itb.ivanperez7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class UnaMoscaGenerator {
    public static void main(String[] args) {
        String song = "una mosca volava per la llum. i la llum es va apagar. i la pobra mosca es va quedar a les fosques i la pobra mosca no va poder volar.";

        Scanner scanner = new Scanner(System.in);
        char vowel = scanner.next().charAt(0);
        song = song.replace('a', vowel)
                .replace('e', vowel)
                .replace('i', vowel)
                .replace('o', vowel)
                .replace('u', vowel);

        System.out.println(song);
    }
}

