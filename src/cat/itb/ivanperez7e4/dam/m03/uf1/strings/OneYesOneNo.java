package cat.itb.ivanperez7e4.dam.m03.uf1.strings;

import java.util.Scanner;

public class OneYesOneNo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        String input = scanner.nextLine();
        for (int i = 0; i < input.length(); i++) {
            if(i%2==0){
                char result = input.charAt(i);
                System.out.print(result);
            }

        }

    }
}
