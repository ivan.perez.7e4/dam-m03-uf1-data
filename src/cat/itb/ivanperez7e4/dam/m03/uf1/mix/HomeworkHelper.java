package cat.itb.ivanperez7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class HomeworkHelper {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        int divident = scanner.nextInt();
        while(divident!=-1) {
            int divisor = scanner.nextInt();
            int quocient = scanner.nextInt();
            int residuo = scanner.nextInt();

            boolean quocientCorrect = divident / divisor == quocient;
            boolean residuoCorrect = divident % divisor == residuo;
            if (quocientCorrect && residuoCorrect)
                System.out.println("correcte");
            else
                System.out.println("error");

            divident = scanner.nextInt();
        }
    }
}
