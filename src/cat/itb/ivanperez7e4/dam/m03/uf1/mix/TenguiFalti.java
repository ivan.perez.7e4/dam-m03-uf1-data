package cat.itb.ivanperez7e4.dam.m03.uf1.mix;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class TenguiFalti {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        List<Integer> repes = new ArrayList<>();
        int cromo = scanner.nextInt();
        while(cromo!=-1) {
            repes.add(cromo);
            cromo = scanner.nextInt();
        }

        List<Integer> faltis = new ArrayList<>();
        cromo = scanner.nextInt();
        while(cromo!=-1) {
            faltis.add(cromo);
            cromo = scanner.nextInt();
        }



        for(int falti : faltis){
            for(int repe : repes){
                if(repe==falti){
                    System.out.printf("%d ",repe);
                    break;
                }
            }
        }

    }
}
