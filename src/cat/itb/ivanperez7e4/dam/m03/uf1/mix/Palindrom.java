package cat.itb.ivanperez7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class Palindrom {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String line = scanner.nextLine();
        // . , ' ! ? -
        line = line.replace(".","")
                .replace(",","")
                .replace("'","")
                .replace("!","")
                .replace("?","")
                .replace("-","")
                .replace(" ","");
        boolean palindrom = true;
        int length = line.length();
        for(int i = 0; i< length/2; ++i){
            if(line.charAt(i)!=line.charAt(length-i-1)){
                palindrom = false;
                break;
            }
        }
        System.out.println(palindrom);
    }
}
