package cat.itb.ivanperez7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class RadarFilter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        int velocidad = scanner.nextInt();
        int contador =0;
        while(velocidad!=-1){
            if(velocidad>90){
                contador++;
            }
            velocidad = scanner.nextInt();
        }
        System.out.println(contador);
    }
}