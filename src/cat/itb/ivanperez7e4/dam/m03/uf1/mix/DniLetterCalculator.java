package cat.itb.ivanperez7e4.dam.m03.uf1.mix;

import java.util.Scanner;

public class DniLetterCalculator {
    public static void main(String[] args) {
        char[] letters = {'T','R','W','A','G','M','Y','F',
                'P','D','X','B','N','J','Z','S','Q','V',
                'H','L','C','K','E'};

        Scanner scanner = new Scanner(System.in);
        int dni = scanner.nextInt();
        int position = dni%23;
        char letter = letters[position];

        System.out.printf("%08d%c", dni, letter);
    }
}

