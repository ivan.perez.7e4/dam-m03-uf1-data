package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class NumberBetweenOneAndFive {
    public static void main(String[] args) {
        System.out.println("Introdueix un enter entre l'1 i el 5");
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();
        int i=0;
        do{
            System.out.println("Introdueix un enter entre l'1 i el 5");
            enter = scanner.nextInt();
        } while (enter<1 || enter>5);
            System.out.println("El número introduït: "+enter);

    }
}
