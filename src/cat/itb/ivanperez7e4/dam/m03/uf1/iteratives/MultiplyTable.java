package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class MultiplyTable {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();

        for(int i=1; i<=9; i++){
            System.out.println(i + "*" + enter + "=" + i*enter);
        }
    }
}
