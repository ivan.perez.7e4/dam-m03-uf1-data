package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives.practice;

import java.util.Scanner;


public class SetIMig {
    public static void main(String[] args) {
        playUserTurn();
    }
    public static String getCardText(int card) {
        switch (card){
            case 8:
                return "sota";
            case 9:
                return "cavall";
            case 10:
                return "rei";
            default:
                return card+"";
        }
    }

    public static double getCardValue(int card){
        switch(card) {
            case 8: case 9: case 10:
                return 0.5;
            default:
                return card;
        }
    }

    public static double playUserTurn(){
        int cartaUsuario=ItbRandomizer.nextInt(1,10);
        System.out.println("T'ha sortit la carta: "+getCardText(cartaUsuario));
        System.out.println("Resultat actual: "+getCardValue(cartaUsuario));
        double contadorUsuario = getCardValue(cartaUsuario);
        System.out.println("Vols seguir jugant? (1-> Si  0-> No)");
        Scanner scanner = new Scanner(System.in);
        int usuario = scanner.nextInt();
        while (usuario==1){
            cartaUsuario=ItbRandomizer.nextInt(1,10);
            System.out.println("T'ha sortit la carta: "+getCardText(cartaUsuario));
            contadorUsuario =contadorUsuario+getCardValue(cartaUsuario);
            if(contadorUsuario>7.5){
                System.out.println("El teu resultat es: "+contadorUsuario);
                System.out.println("T'has passat.");
                System.exit(0);
            } else{
                System.out.println("Resultat actual: "+contadorUsuario);
                System.out.println("Vols seguir jugant? (1-> Si  0-> No)");
                usuario = scanner.nextInt();
            }
        }

        System.out.println("El teu resultat és: "+contadorUsuario);
        playComputerTurn(contadorUsuario);
        return (contadorUsuario);
    }

    public static double playComputerTurn(double contadorUsuario){
        double contadorOrdenador =0;
        while(contadorOrdenador<7.5 && contadorOrdenador < contadorUsuario){
            int cartaOrdenador=ItbRandomizer.nextInt(1,10);
            contadorOrdenador+=getCardValue(cartaOrdenador);
            System.out.println("L'ordinador ha tret un: "+cartaOrdenador);
        }
        if(contadorOrdenador>7.5){
            System.out.println("L'ordinador ha tret un: "+contadorOrdenador);
            System.out.println("L'ordinador s'ha passat");
            System.out.println("Has guanyat!!!");
        }
        else if(contadorOrdenador<=7.5 && contadorOrdenador>contadorUsuario){
            System.out.println("L'ordinador ha tret un: "+contadorOrdenador);
            System.out.println("Ha guanyat la banca");
        }
        else if(contadorOrdenador<=7.5 && contadorOrdenador<contadorUsuario){
            System.out.println("L'ordinador ha tret un: "+contadorOrdenador);
            System.out.println("Has guanyat!!!");
        }
        return contadorOrdenador;
    }
}
