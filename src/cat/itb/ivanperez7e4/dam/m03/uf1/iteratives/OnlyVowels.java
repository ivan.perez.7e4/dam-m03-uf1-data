package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class OnlyVowels {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Indica la quantitat de lletres y despres posa'ls amb espais and blanc");
        int quantitat = scanner.nextInt();
        scanner.nextLine();

        for(int i=0;i<quantitat; ++i){
            char letra=scanner.nextLine().charAt(0);
            if (letra=='a' ||letra=='e'||letra=='i'||letra=='o'||letra=='u'){
                System.out.println(letra);
            }
        }
    }
}
