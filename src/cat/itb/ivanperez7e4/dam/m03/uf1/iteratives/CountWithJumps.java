package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class CountWithJumps {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enterFinal = scanner.nextInt();
        int enterSalt = scanner.nextInt();

        for(int i=1; enterFinal>=i; i=i+enterSalt){
            System.out.println(i);
        }
    }
}
