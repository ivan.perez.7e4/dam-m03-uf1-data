package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class HowManyLines {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();

        int i=0;

        while(!text.equals("END")) {
            text = scanner.nextLine();
            i++;
        }
        System.out.println(i);

    }
}
