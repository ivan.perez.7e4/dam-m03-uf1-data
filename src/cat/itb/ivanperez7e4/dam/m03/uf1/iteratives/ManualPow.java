package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class ManualPow {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int base = scanner.nextInt();
        int exponent = scanner.nextInt();
        System.out.println(pow(base,exponent));
        }

    private static int pow(int base, int exponent) {
        int pow = 1;
        for(int i=0;i<exponent;i++)
            pow*=base;
        return pow;
    }
}
