package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class CountDown {
    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();

        for(int i=0; enter!=i; enter--){
            System.out.print(enter);

        }
    }
}

