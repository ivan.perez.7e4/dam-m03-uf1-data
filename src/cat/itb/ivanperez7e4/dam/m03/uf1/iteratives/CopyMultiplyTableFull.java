package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

public class CopyMultiplyTableFull {
    public static void main(String[] args) {
        int tabla = 1;
        do {
            int contador = 1;
            do {
                int result = contador * tabla;
                System.out.print(result + " ");
                contador++;
            } while (contador <= 9);
            System.out.println();
            tabla++;
        } while (tabla <= 9);
    }
}