package cat.itb.ivanperez7e4.dam.m03.uf1.iteratives;

import java.util.Scanner;

public class WaitFor5 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();

        while(enter!=5){
            enter = scanner.nextInt();
        }
        System.out.println("5 trobat!");
    }
}
