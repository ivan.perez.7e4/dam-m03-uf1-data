package cat.itb.ivanperez7e4.dam.m03.uf1.arrays.exam;

import cat.itb.ivanperez7e4.dam.m03.uf1.arrays.ArrayReader;

import java.util.Scanner;

public class SumPositiveValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] value = ArrayReader.scannerReadIntArray(scanner);
        int suma=0;
        for (int i = 0; i < value.length ; i++) {
            if(value[i]>=1) {
                suma += value[i];
            }
        }
        System.out.println(suma);
    }
}
