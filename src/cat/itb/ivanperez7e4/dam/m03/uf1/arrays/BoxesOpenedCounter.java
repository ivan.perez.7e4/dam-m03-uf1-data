package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class BoxesOpenedCounter {
    public static void main(String[] args) {
            int[] value = new int[10];
            Scanner scanner = new Scanner(System.in);
            int entrada = scanner.nextInt();

            while(entrada!=-1){
                value[entrada]++;
                entrada=scanner.nextInt();
            }
            System.out.println(Arrays.toString(value));
        }

    }

