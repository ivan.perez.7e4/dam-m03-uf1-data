package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class PushButtonPadlockSimulator {
    public static void main(String[] args) {
        boolean[] value = new boolean[8];
        Scanner scanner = new Scanner(System.in);
        int entrada = scanner.nextInt();

        while(entrada!=-1){
           value[entrada] = !value[entrada];
           entrada=scanner.nextInt();
        }
        System.out.println(Arrays.toString(value));
    }

}
