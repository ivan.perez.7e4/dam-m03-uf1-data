package cat.itb.ivanperez7e4.dam.m03.uf1.arrays.exam;

import java.util.Arrays;
import java.util.Scanner;

public class ArrayConfigurator {
    public static void main(String[] args) {
        int[] array = new int [10];
        Scanner scanner = new Scanner(System.in);
        int posicion = scanner.nextInt();
        int valor = scanner.nextInt();
        while(posicion!=-1) {
                array[posicion]=valor;
                posicion=scanner.nextInt();
                valor = scanner.nextInt();
        }
        System.out.println(Arrays.toString(array));
    }
}
