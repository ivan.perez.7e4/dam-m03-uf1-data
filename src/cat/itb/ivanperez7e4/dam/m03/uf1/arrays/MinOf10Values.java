package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Collections;
import java.util.Scanner;

public class MinOf10Values {
    public static void main(String[] args) {
        int[] values = new int [10];
        Scanner scanner = new Scanner(System.in);
        for(int i=0; i<10;++i) {
            int value = scanner.nextInt();
            values[i] = value;
        }
        int minValues = values[0];
        for(int i=0; i<10;++i){
            minValues = Math.min(minValues, values[i]);

        } System.out.println(minValues);
    }
}

