package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class BicicleDistance {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        double velocidad = scanner.nextDouble();
        int[] values = {1,2,3,4,5,6,7,8,9,10};
        for(int i=0; i<=values.length; ++i) {
            double result = values[i]*velocidad;
            System.out.println(result);

        }
    }
}
