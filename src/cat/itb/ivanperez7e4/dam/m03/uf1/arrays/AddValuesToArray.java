package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;

public class AddValuesToArray {
    public static void main(String[] args) {
        float[] value = new float[50];
        value[0] = 31.0f;
        value[1] = 56.0f;
        value[19] = 12.0f;
        value[49] = 79.0f;
        System.out.println(Arrays.toString(value));
    }
}
