package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class IvaPrices {
    public static void main(String[] args) {
        float[] values = new float[10];
        Scanner scanner = new Scanner(System.in);
        for(int i=0; i<10;++i) {
            float value = scanner.nextInt();
            values[i] = value;
        }
        for(int i=0; i<values.length; ++i) {
            double result = values[i]*0.21;
            double result1 = values[i]+result;
            System.out.println(values[i]+" IVA = "+result1);
        }

    }
}
