package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixBoxesOpenedCounter {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int [][] matrix = new int[4][4];

        int row = scanner.nextInt();
        int column = scanner.nextInt();

        while(row!=-1 || column!=-1) {
            matrix[row][column]++;
            row = scanner.nextInt();
            column = scanner.nextInt();
        }
        System.out.println(Arrays.deepToString(matrix));
    }
}
