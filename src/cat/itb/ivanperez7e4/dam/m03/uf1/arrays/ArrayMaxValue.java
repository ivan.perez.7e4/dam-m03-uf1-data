package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArrayMaxValue {
    public static void main(String[] args) {
       Scanner scanner = new Scanner(System.in);
       int[] value = ArrayReader.scannerReadIntArray(scanner);

        int maxValues = value[0];
        for(int i=0; i<value.length;++i){
            maxValues = Math.max(maxValues, value[i]);

        } System.out.println(maxValues);
    }
}


