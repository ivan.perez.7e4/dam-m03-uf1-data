package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;

public class HighestMountainScaleChange {
    public static void main(String[] args) {
        double[][] map ={{1.5,1.6,1.8,1.7,1.6},{1.5,2.6,2.8,2.7,1.6},{1.5,4.6,4.4,4.9,1.6},{2.5,1.6,3.8,7.7,3.6},{1.5,2.6,3.8,2.7,1.6}};
        double highestPoint = map[0][0];
        int x = 0;
        int y = 0;
        for(int i = 0 ; i<map.length; ++i){
            for(int j = 0; j<map[i].length; ++j){
                map[i][j]*=3.2808;
            }
        }
        System.out.println(Arrays.deepToString(map));
    }
}