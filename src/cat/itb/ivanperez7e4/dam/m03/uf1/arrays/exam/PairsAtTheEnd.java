package cat.itb.ivanperez7e4.dam.m03.uf1.arrays.exam;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class PairsAtTheEnd {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int valor = scanner.nextInt();
        List<Integer> list = new ArrayList<Integer>();

        while(valor!=-1){
            if(valor%2==0) {
                list.add(0,valor);
                valor = scanner.nextInt();
            }
            else {list.add(valor);
                valor = scanner.nextInt();
            }
        }
        System.out.println(list);
    }
}
