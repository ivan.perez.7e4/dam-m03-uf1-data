package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ValueNearAvg {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] value = ArrayReader.scannerReadIntArray(scanner);

        int suma = 0;
        for (int i = 0; i < value.length; ++i) {
            suma += value[i];
        }
        int media = suma / value.length;

        int minValor = value[0];
        for (int j = 0; j < value.length; j++) {
            int resta = media - value[j];
            minValor = Math.min(resta, value[j]);


        }
    }
}