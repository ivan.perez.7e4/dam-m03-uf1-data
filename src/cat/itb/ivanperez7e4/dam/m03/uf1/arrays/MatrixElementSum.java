package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Arrays;
import java.util.Scanner;

public class MatrixElementSum {
    public static void main(String[] args) {
        int[][] matrix = {{2,5,1,6},{23,52,14,36},{23,75,81,64}};
        int sum=0;
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                int matrix1= matrix[i][j];
                sum+=matrix1;
            }
        } System.out.println(sum);
    }
}
