package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CapICuaValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] value = ArrayReader.scannerReadIntArray(scanner);
        boolean isCapICua = true;
        for(int i=0; i<value.length; ++i){
            int cap = value[i];
            int cua = value[value.length-1-i];
            if(cap!=cua){
                isCapICua = false;
                System.out.println("No es cap i cua");
                break;
            }
        }
        if (isCapICua)
            System.out.println("cap i cua");
    }
}
