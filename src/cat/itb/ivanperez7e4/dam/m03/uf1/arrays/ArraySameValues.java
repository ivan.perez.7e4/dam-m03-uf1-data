package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySameValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] value = ArrayReader.scannerReadIntArray(scanner);

        int suma =0;
        for(int i=0; i<value.length; ++i){
            suma +=value[i];
        }
        System.out.println(suma);
    }
}
