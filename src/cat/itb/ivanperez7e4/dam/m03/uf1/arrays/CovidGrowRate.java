package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class CovidGrowRate {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] value = ArrayReader.scannerReadIntArray(scanner);
        for(int i=1; i<value.length; ++i) {
            double current = value[i];
            double previous = value[i-1];
            double rate = current/previous;
            System.out.println(rate);
        }
    }
}
