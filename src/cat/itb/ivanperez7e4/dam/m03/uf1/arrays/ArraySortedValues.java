package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class ArraySortedValues {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] values = ArrayReader.scannerReadIntArray(scanner);
        boolean sorted = true;
        for (int i = 0; i + 1 < values.length; ++i) {
            int value = values[i];
            int nextValue = values[i + 1];
            if (nextValue < value) {
                sorted = false;
                break;
            }
        }
        if (sorted)
            System.out.println("ordenat");
    }
}
