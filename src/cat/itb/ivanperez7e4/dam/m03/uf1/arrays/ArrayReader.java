package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;
import java.util.Scanner;

public class ArrayReader {

    public static int[] scannerReadIntArray(Scanner scanner) {
        int size = scanner.nextInt();
        int[] values = new int[size];

        for (int i = 0; i < size; ++i) {
            values[i] = scanner.nextInt();
        }
        return values;
    }


//    public static int[][] scannerReadIntMatrix(Scanner scanner) {
//        int size = scanner.nextInt();
//        int size1 = scanner.nextInt();
//        int[][] values = new int[size][size1];
//
//        for (int i = 0; i < size; ++i) {
//            for (int j = 0; j <size1 ; j++) {
//                size = scanner.nextInt();
//                size1 = scanner.nextInt();
//                return values[i][j];
//            }
//        }
//
//    }
//
//
    }

