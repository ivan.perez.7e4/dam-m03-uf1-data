package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

import java.util.Scanner;

public class InverseOrder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] valor = new int[10];
        for(int i=0; i<10; ++i){
          valor[i]= scanner.nextInt();
        }
        for(int i=9; i>=0; --i){
            int value=valor[i];
            System.out.println(value);
        }
    }
}
