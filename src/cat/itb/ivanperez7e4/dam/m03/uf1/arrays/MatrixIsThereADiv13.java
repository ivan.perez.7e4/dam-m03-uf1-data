package cat.itb.ivanperez7e4.dam.m03.uf1.arrays;

public class MatrixIsThereADiv13 {
    public static void main(String[] args) {
        int[][] matrix = {{2, 5, 1, 6}, {23, 52, 14, 36}, {23, 75, 81, 62}};
        boolean naisteperet = false;
        for (int i = 0; i < matrix.length; ++i) {
            for (int j = 0; j < matrix[i].length; j++) {
                if (matrix[i][j] % 13 == 0) {
                    naisteperet = true;
                    break;
                }
            }
        } System.out.println(naisteperet);
    }
}