package cat.itb.ivanperez7e4.dam.m03.uf1.generalexam;

import java.util.Arrays;
import java.util.Scanner;

public class BasketballTournamentManager {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int[] value = new int[8];
        int victorias = scanner.nextInt();

        while(victorias!=-1){
            value[victorias]++;
            victorias = scanner.nextInt();
            System.out.println("L'equip "+victorias+" té "+value[victorias]+" victòries");
            int maxValues = value[0];
            for(int i=0; i<value.length;++i) {
                maxValues = Math.max(maxValues, value[i]);            }
        }

    }
}
