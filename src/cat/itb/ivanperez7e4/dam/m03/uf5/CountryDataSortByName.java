package cat.itb.ivanperez7e4.dam.m03.uf5;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Scanner;

public class CountryDataSortByName {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countriesList = readCountries(scanner);
        countriesList.stream()
                .filter(country -> country.getDensitat() >5)
                .sorted(Comparator.comparing(Country::getNom))
                .forEach(System.out::println);

    }

    public static List<Country> readCountries(Scanner scanner) {
        int contador = scanner.nextInt();
        List<Country> countriesList2 = new ArrayList<>();
        for (int i = 0; i < contador ; i++) {
            Country country = readCountry(scanner);
            countriesList2.add(country);
        }
        return countriesList2;
    }

    public static Country readCountry(Scanner scanner) {
        String nom = scanner.next();
        String capital = scanner.next();
        int superficie = scanner.nextInt();
        int densitat = scanner.nextInt();
        return new Country(nom, capital, superficie, densitat);
    }
}
