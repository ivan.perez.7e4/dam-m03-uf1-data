package cat.itb.ivanperez7e4.dam.m03.uf5;

import java.util.Arrays;
import java.util.Comparator;
import java.util.List;

public class SortByLastDigit {
    public static void main(String[] args) {
        List<Integer> list = Arrays.asList(253, 432, 65, 234, 43, 16, 28, 432, 34, 65, 312, 34, 2134, 76, 2, 76, 23, 67, 27, 8, 54235256, 4560, 7431);

        list.sort(Comparator.comparing(cat.itb.ivanperez7e4.dam.m03.uf5.SortByLastDigit::getLastDigit));
        System.out.println(list);
    }

    private static int getLastDigit(Integer integer) {
        return integer%10;
    }
}
