package cat.itb.ivanperez7e4.dam.m03.uf5;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MetersToCentimeters {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        list.stream()
                .map(value -> value*1000)
                .forEach(System.out::println);
    }
}
