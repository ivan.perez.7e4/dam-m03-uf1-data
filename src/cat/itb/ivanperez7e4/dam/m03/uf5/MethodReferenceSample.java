package cat.itb.ivanperez7e4.dam.m03.uf5;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MethodReferenceSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

        list.removeIf(cat.itb.ivanperez7e4.dam.m03.uf5.MethodReferenceSample::endsWithThree);
        list.sort(cat.itb.ivanperez7e4.dam.m03.uf5.MethodReferenceSample::mayorMenor);
        list.forEach(System.out::println);
    }

    private static int mayorMenor(Integer integer, Integer integer1) {
        return integer1-integer;
    }

    private static boolean endsWithThree(Integer integer) {
        return integer%10==3;
    }
}
