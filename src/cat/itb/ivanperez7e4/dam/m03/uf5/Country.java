package cat.itb.ivanperez7e4.dam.m03.uf5;

public class Country {
    String nom;
    String capital;
    int superficie;
    int densitat;

    public Country(String nom, String capital, int superficie, int densitat) {
        this.nom = nom;
        this.capital = capital;
        this.superficie = superficie;
        this.densitat = densitat;
    }


    public String getNom() {
        return nom;
    }

    public String getCapital() {
        return capital;
    }

    public int getSuperficie() {
        return superficie;
    }

    public int getDensitat() {
        return densitat;
    }

    @Override
    public String toString() {
        return "Country{" +
                "nom='" + nom + '\'' +
                ", capital='" + capital + '\'' +
                ", superficie=" + superficie +
                ", densitat=" + densitat +
                '}';
    }

    public double getAverage() {
        return superficie/densitat;
    }
}
