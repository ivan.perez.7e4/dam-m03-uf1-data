package cat.itb.ivanperez7e4.dam.m03.uf5;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class LamdaSample {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);

        list.removeIf(value -> value%10==3);
        list.sort((v1, v2) -> v2-v1);
        list.forEach(value -> System.out.println(value));
    }
}
