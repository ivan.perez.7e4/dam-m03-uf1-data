package cat.itb.ivanperez7e4.dam.m03.uf5;

import cat.itb.ivanperez7e4.dam.m03.uf4.figures.ConsoleColors;
import cat.itb.ivanperez7e4.dam.m03.uf4.figures.Figure;
import cat.itb.ivanperez7e4.dam.m03.uf4.figures.LeftPiramidFigure;
import cat.itb.ivanperez7e4.dam.m03.uf4.figures.RectangleFigure;

import java.util.ArrayList;
import java.util.List;

public class PainterGreen {
    public static void main(String[] args) {
        List<Figure> figures = new ArrayList<>();

        figures.add(new RectangleFigure(ConsoleColors.RED, 4, 5));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 3));
        figures.add(new RectangleFigure(ConsoleColors.GREEN, 3, 5));
        figures.add(new RectangleFigure(ConsoleColors.BLUE, 1, 1));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 4));
        figures.add(new RectangleFigure(ConsoleColors.GREEN, 10, 5));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 8));

//Imprimeix per pantalla només les que no siguin verdes.
//        figures.stream().filter(figure ->!figure.getColor().equals(ConsoleColors.GREEN))
//                .forEach(System.out::println);
    }
}
