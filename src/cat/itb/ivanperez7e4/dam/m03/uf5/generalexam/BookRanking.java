package cat.itb.ivanperez7e4.dam.m03.uf5.generalexam;

import java.util.*;

public class BookRanking {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Llibre> list = readBooks(scanner);

        Llibre llibre1 = Collections.max(list, Comparator.comparing(Llibre::getNumPag));
        System.out.println("### Llibre amb més pàgines "+llibre1);

        System.out.println("### Llibres per any ");
        orderByYear(list);

        double llibre3 = avgPag(list);
        System.out.println("### Mitjana de pàgines "+llibre3);

        System.out.println("### Llibres publicats després del 2018 ");
        publicacio(list);

        System.out.println("### Més de 100 pàgines ");
        ordenAlfabetico(list);

        //no lo he dejado muy bonito porque preferia dedicarle mas tiempo a los ejercicios

    }
    private static void ordenAlfabetico(List<Llibre> list) {
        list.stream().filter(llibre -> llibre.getNumPag()>100)
                     .sorted(Comparator.comparing(Llibre::getTitol))
                     .forEach(System.out::println);
    }

    private static void publicacio(List<Llibre> list) {
        list.stream().filter(llibre -> llibre.getAny()>2018)
                     .forEach(System.out::println);
    }

    private static double avgPag(List<Llibre> list) {
        double average = list.stream()
                .mapToInt(llibre -> llibre.getNumPag())
                .average().getAsDouble();
        return average;
    }

    private static void orderByYear(List<Llibre> list) {
        list.stream().sorted(Comparator.comparing(Llibre::getAny)
                        .thenComparing(Llibre::getTitol))//no me dio tiempo pero para ordearlo de mayor a menor llibre1 -llibre
                .forEach(System.out::println);

    }

    private static List<Llibre> readBooks(Scanner scanner) {
        int count = scanner.nextInt();
        List<Llibre> list = new ArrayList<>();
        for (int i = 0; i < count; i++) {
            Llibre book = (Llibre) readBook(scanner);
            list.add(book);
        }
        return list;
    }

    private static Object readBook(Scanner scanner) {
        scanner.next();
        String titol = scanner.nextLine();
        String autor = scanner.nextLine();
        String ISBN = scanner.nextLine();
        int numPag = scanner.nextInt();
        int any = scanner.nextInt();
        return new Llibre(titol,autor, ISBN, numPag, any);
    }
}
