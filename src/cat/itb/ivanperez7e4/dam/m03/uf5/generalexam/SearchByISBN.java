package cat.itb.ivanperez7e4.dam.m03.uf5.generalexam;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class SearchByISBN {
        public static void main(String[] args) {
            Scanner scanner = new Scanner(System.in);
            int count = scanner.nextInt();
            Map<String, Llibre> bookMap = new HashMap<>();
            for (int i = 0; i < count; i++) {
                Llibre book = readBook(scanner);
                bookMap.put(book.getISBN(), book);
            }

            int countISBN = scanner.nextInt();
            for (int i = 0; i < countISBN; i++) {
                String input = scanner.nextLine();
                Llibre book = bookMap.get(input);
                System.out.println(book);
                scanner.nextLine();
            }
        }

        private static Llibre readBook(Scanner scanner) {
            scanner.next();
            String titol = scanner.nextLine();
            String autor = scanner.nextLine();
            String ISBN = scanner.nextLine();
            int numPag = scanner.nextInt();
            int any = scanner.nextInt();
            return new Llibre(titol,autor, ISBN, numPag, any);
        }
    }

