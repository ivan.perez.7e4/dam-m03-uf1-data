package cat.itb.ivanperez7e4.dam.m03.uf5.generalexam;

public class Llibre {
    String titol;
    String autor;
    String ISBN;
    int numPag;
    int any;

    public Llibre(String titol, String autor, String ISBN, int numPag, int any) {
        this.titol = titol;
        this.autor = autor;
        this.ISBN = ISBN;
        this.numPag = numPag;
        this.any = any;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public String getISBN() {
        return ISBN;
    }

    public void setISBN(String ISBN) {
        this.ISBN = ISBN;
    }

    public int getNumPag() {
        return numPag;
    }

    public void setNumPag(int numPag) {
        this.numPag = numPag;
    }

    public int getAny() {
        return any;
    }

    public void setAny(int any) {
        this.any = any;
    }

    @Override
    public String toString() {
        return titol+" - "+autor+" - "+any;
    }
}
