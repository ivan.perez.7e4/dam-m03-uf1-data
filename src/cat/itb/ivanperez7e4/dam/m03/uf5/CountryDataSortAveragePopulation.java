package cat.itb.ivanperez7e4.dam.m03.uf5;

import java.util.List;
import java.util.Scanner;

public class CountryDataSortAveragePopulation {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Country> countriesList = CountryDataSortByName.readCountries(scanner);
        countriesList.stream()
                .filter(country -> country.getSuperficie()<1200000)
                .map(Country::getAverage)
                .forEach(System.out::println);
    }
}
