package cat.itb.ivanperez7e4.dam.m03.uf5;

import cat.itb.ivanperez7e4.dam.m03.uf2.dataclasses.MultiProductInfoPrinter;
import cat.itb.ivanperez7e4.dam.m03.uf2.dataclasses.Product;
import java.util.*;

public class MostExpensiveProduct {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Product> readProducts = MultiProductInfoPrinter.readProducts(scanner);
        Product product = Collections.max(readProducts, Comparator.comparing(Product::getPrice));
        System.out.printf("El producte %s val %.2f", product.getName(), product.getPrice());
    }
}
