package cat.itb.ivanperez7e4.dam.m03.uf5.practica;

import java.io.IOException;
import java.nio.file.Paths;
import java.util.*;

public class CovidStatsApp{
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(Paths.get("./src/cat/itb/ivanperez7e4/dam/m03/uf5/practica/coviddata.txt"));
        List<Dades> countries = readDatas(scanner);
        printTotals(countries);
        printTops(countries);
        printTopEU(countries);
        printTopEuPopulation(countries); }

    private static void printTopEuPopulation(List<Dades> countries) throws IOException {
        countries.removeIf(cat.itb.ivanperez7e4.dam.m03.uf5.practica.CovidStatsApp::isEuropean);
        //Aqui no se como hacerlo, se que tengo que coger el total confirmados
        // dividirlo entre population y *100 pero no se como hacerlo
        System.out.println("### Relative Tops EU ###");
//        System.out.printf("Païs amb més casos nous: s%", maxNewCasesByPopulation);
//        System.out.printf("\nPaïs amb més casos totals: s%", maxTotalCasesByPopulation);
    }



    private static void printTopEU(List<Dades> countries) {
        countries.removeIf(cat.itb.ivanperez7e4.dam.m03.uf5.practica.CovidStatsApp::isEuropean);
        String maxNewCases = Collections.max(countries, Comparator.comparing(Dades::getNewCases)).getCountry();
        String maxTotalCases = Collections.max(countries, Comparator.comparing(Dades::getTotalCases)).getCountry();
        System.out.println("### Tops EU ###");
        System.out.print("Païs amb més casos nous: "+ maxNewCases);
        System.out.println("\nPaïs amb més casos totals: "+ maxTotalCases);
    }

    private static boolean isEuropean(Dades country) {
        String[] ids = {"BE", "EL", "LT", "PT", "BG", "ES", "LU", "RO", "CZ", "FR", "HU", "SI", "DK", "HR", "MT", "SK", "DE", "IT", "NL", "FI", "EE", "CY", "AT", "SE", "IE", "LV", "PL"};
        for (String id : ids){
            if (country.getCountryCode().equals(id)){
                return false;
            }
        }
        return true;
    }

    private static Dades maxTotalCases(List<Dades> countries) {
        return Collections.max(countries, Comparator.comparing(Dades::getTotalCases));
    }

    private static Dades maxNewCases(List<Dades> countries) {
        return Collections.max(countries, Comparator.comparing(Dades::getNewCases));
    }


    private static void printTotals(List<Dades> countries) {
        int newCases = countries.stream()
                            .mapToInt(dades -> dades.getNewCases())
                            .reduce(0,Integer::sum);

        int totalCases = countries.stream()
                              .mapToInt(dades -> dades.getTotalCases())
                              .reduce(0,Integer::sum);

            System.out.println("### DADES TOTALS ###");
            System.out.println("Casos nous: "+newCases);
            System.out.println("Casos totals: "+totalCases);
            System.out.println("");
        }
    private static void printTops(List<Dades> countries) {
        String maxNewCases = Collections.max(countries, Comparator.comparing(Dades::getNewCases)).getCountry();
        String maxTotalCases = Collections.max(countries, Comparator.comparing(Dades::getTotalCases)).getCountry();

        System.out.println("### TOPS ###");
        System.out.println("Païs amb més casos nous: "+maxNewCases);
        System.out.println("Païs amb més casos totals: "+maxTotalCases);
    }

    private static List<Dades> readDatas(Scanner scanner) {
        List<Dades> list = new ArrayList<>();
        while(scanner.hasNext()){
            Dades covidCountryData = readData(scanner);
            list.add(covidCountryData);
            }
            return list;
        }

    private static Dades readData(Scanner scanner) {
        String country = scanner.nextLine();
        String countryCode = scanner.nextLine();
        int newCases = scanner.nextInt();
        int totalCases = scanner.nextInt();
        int newDeaths = scanner.nextInt();
        int totalDeaths = scanner.nextInt();
        int newRecovered = scanner.nextInt();
        int totalRecovered = scanner.nextInt();
        scanner.nextLine();
        return new Dades(country, countryCode, newCases, totalCases, newDeaths, totalDeaths, newRecovered, totalRecovered);
        }
}