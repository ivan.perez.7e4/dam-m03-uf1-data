package cat.itb.ivanperez7e4.dam.m03.uf5.regex;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class DificultPassword {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String regex1 = "@#~$";
        while(!input.equals("END")){
            String regex = "(\\w+)-M(\\d+)UF(\\d+)";
            Pattern pattern = Pattern.compile(regex);
            Matcher matcher = pattern.matcher(input);
            while (matcher.find()) {
                System.out.println("Estàs cursant la unitat formativa "+matcher.group(1)+", del mòdul "+matcher.group(2)+" de "+matcher.group(3));
            }
            scanner.nextLine();
        }
    }
}
