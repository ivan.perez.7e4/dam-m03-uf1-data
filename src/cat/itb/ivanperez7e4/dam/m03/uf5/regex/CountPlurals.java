package cat.itb.ivanperez7e4.dam.m03.uf5.regex;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CountPlurals {
    public static void main(String[] args) {
        String lorem = "Lorem ipsum dolors sit amet, consectetur adipisicis elit, sed eiusmod tempors incidunt ut labore et dolores magna aliqua.";
        String regex = "s ";
        Pattern pattern = Pattern.compile(regex);
        Matcher matcher = pattern.matcher(lorem);
        int count = 0;
        while (matcher.find()) {
            count++;
        }
        System.out.println(count);
    }
}
