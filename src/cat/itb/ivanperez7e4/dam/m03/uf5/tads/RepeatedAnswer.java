package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class RepeatedAnswer {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<String> fruta = new HashSet<>();
        String input = scanner.next();

        while(!input.equals("END")){
            if (fruta.contains(input)){
                System.out.println("MEEEC!");
            }
            else fruta.add(input);
            input = scanner.next();
        }
    }
}
