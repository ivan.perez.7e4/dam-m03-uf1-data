package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

import java.util.HashSet;
import java.util.Scanner;
import java.util.Set;

public class Bingo {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Set<Integer> bingo = new HashSet<>();
        int input = scanner.nextInt();

        for (int i = 0; i < 10; i++) {
            bingo.add(input);
            input = scanner.nextInt();
        }

        while (!bingo.isEmpty()) {
            int input2 = scanner.nextInt();
            bingo.remove(input2);
        }
        System.out.println("BINGO");
    }
}

