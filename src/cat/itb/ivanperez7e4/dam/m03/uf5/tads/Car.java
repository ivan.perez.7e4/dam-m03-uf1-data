package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

import java.util.Map;

public class Car {
    String matricula;
    String marca;
    String color;

    public Car(String matricula, String marca, String color) {
        this.matricula = matricula;
        this.marca = marca;
        this.color = color;
    }

    public String getMatricula() {
        return matricula;
    }

    public String getMarca() {
        return marca;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return matricula+" "  + marca+" " + color;
    }
}
