package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class EmpoyeeById {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        Map<String, Employee> employeeMap = new HashMap<>();
        for (int i = 0; i < count; i++) {
            Employee employee = readEmployee(scanner);
            employeeMap.put(employee.getDni(), employee);
        }

        String dni = scanner.next();
        while (!dni.equals("END")){
            Employee employee = employeeMap.get(dni);
            System.out.println(employee);
            dni = scanner.next();
        }
    }

    public static Employee readEmployee(Scanner scanner) {
        String dni = scanner.next();
        String name = scanner.next();
        String surname = scanner.next();
        scanner.nextLine();
        String address = scanner.nextLine();
        return new Employee(dni, name, surname, address);
    }
}

