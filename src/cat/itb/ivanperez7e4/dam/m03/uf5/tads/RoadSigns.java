package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class RoadSigns {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        Map<Integer, String> signs = readSigns(scanner);
        printSigns(signs, scanner);
    }

    private static void printSigns(Map<Integer, String> signs, Scanner scanner) {
        int meters = scanner.nextInt();
        while (meters!=-1){
            String sign = signs.getOrDefault(meters, "no hi ha cartell");
            //getOrDefault si el valor que metes no es el mismo del Map signs, te devuelvo el defaultValue
            System.out.println(sign);
            meters = scanner.nextInt();
        }
    }

    private static Map<Integer, String> readSigns(Scanner scanner) {
        Map<Integer, String> signs = new HashMap<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            int meters = scanner.nextInt();
            String sign = scanner.nextLine();
            signs.put(meters, sign);
        }
        return signs;
    }
}

