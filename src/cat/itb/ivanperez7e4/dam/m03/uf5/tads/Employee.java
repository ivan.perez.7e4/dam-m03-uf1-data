package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

public class Employee {
    String dni;
    String name;
    String surname;
    String address;

    public Employee(String dni, String name, String surname, String address) {
        this.dni = dni;
        this.name = name;
        this.surname = surname;
        this.address = address;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return name +" "+surname+" - "+dni+", "+address;
    }
}
