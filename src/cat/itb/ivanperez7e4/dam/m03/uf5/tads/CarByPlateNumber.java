package cat.itb.ivanperez7e4.dam.m03.uf5.tads;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class CarByPlateNumber {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int count = scanner.nextInt();
        Map<String, Car> carMap = new HashMap<>();
        for (int i = 0; i < count; i++) {
            Car car = readCar(scanner);
            carMap.put(car.getMatricula(), car);
        }

        String matricula = scanner.next();
        while (!matricula.equals("END")) {
            Car car = carMap.get(matricula);
            System.out.println(car);

            matricula = scanner.next();
        }
    }

    private static Car readCar(Scanner scanner) {
        String matricula = scanner.next();
        String marca = scanner.next();
        String color = scanner.next();
        return new Car(matricula, marca, color);
    }
}
