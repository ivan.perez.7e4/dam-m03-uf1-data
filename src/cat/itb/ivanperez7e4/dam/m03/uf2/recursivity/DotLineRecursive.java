package cat.itb.ivanperez7e4.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class DotLineRecursive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int enter = scanner.nextInt();
        String x =funcionRecursiva(enter);
        System.out.println(x);
    }

    private static String funcionRecursiva(int enter) {
        if (enter==0)
            return "";
        return "."+funcionRecursiva(enter-1);
    }

}
