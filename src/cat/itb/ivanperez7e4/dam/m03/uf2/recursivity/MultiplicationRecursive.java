package cat.itb.ivanperez7e4.dam.m03.uf2.recursivity;

import java.util.Scanner;

public class MultiplicationRecursive {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int n = scanner.nextInt();
        int m= scanner.nextInt();
        int x = multiply(n,m);
        System.out.println(x);
    }

    public static int multiply(int n, int m) {
        if(m==1)
            return n;
        return multiply(n, m - 1) + n;
    }
}