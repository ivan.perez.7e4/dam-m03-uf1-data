package cat.itb.ivanperez7e4.dam.m03.uf2.practica;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class HighPeaks {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Peak> peaks = readPeaks(scanner);
        printPeaksDetails(peaks);

    }

    private static void printPeaksDetails(List<Peak> peaks) {
        System.out.println("------------------------\n" +
                "--- Cims aconseguits ---\n" +
                "------------------------");
        printPeakList(peaks);
        System.out.println("------------------------");
        printTotal(peaks);
        printHighest(peaks);
        printQuickest(peaks);
        printFastest(peaks);
        System.out.println("------------------------");
    }

    private static void printFastest(List<Peak> peaks) {
        Peak fastPeak = peaks.get(0);
        for (Peak peak : peaks){
            if (peak.printAvg()>fastPeak.printAvg()){
                fastPeak = peak;
            }
        }
        System.out.printf("Cim més veloç: %s %skm/hora \n",fastPeak.getName(),String.format("%.2f", fastPeak.printAvg()));
    }

    private static void printQuickest(List<Peak> peaks) {

            Peak quickPeak = peaks.get(0);
            for (Peak peak : peaks){
                if (peak.getTiempo()<quickPeak.getTiempo()){
                    quickPeak = peak;
                }
            }
            System.out.printf("Cim més ràpid: %s (%s) \n", quickPeak.getName(), quickPeak.converterMins() );

    }

    private static void printTotal(List<Peak> peakList) {
        int total = 0;
        for (Peak peak : peakList) {
            total++;
        }
        System.out.println("N. cims: " + total);
    }
        private static void printHighest(List<Peak> peaks) {
            Peak HighestPeak = peaks.get(0);
            for (Peak peak : peaks){
                if (peak.getAltura()> HighestPeak.getAltura()){
                    HighestPeak = peak;
                }
            }
            System.out.printf("Cim més alt: %s (%dm) \n", HighestPeak.getName(), HighestPeak.getAltura() );

        }

    private static void printPeakList(List<Peak> peaks) {
        for(Peak mountain: peaks){
            printMountain(mountain);
        }
    }
    private static void printMountain(Peak mountain) {
        System.out.println(mountain);
    }

    private static List<Peak> readPeaks(Scanner scanner) {
            List<Peak> peaksList = new ArrayList<>();
            int size = scanner.nextInt();
            for(int i=0; i<size; ++i){
                Peak peak = readPeak(scanner);
                peaksList.add(peak);
            }
            return peaksList;

    }

    private static Peak readPeak(Scanner scanner) {
        String name=scanner.next();
        scanner.nextLine();
        int altura= scanner.nextInt();
        String pais=scanner.next();
        scanner.nextLine();
        double distancia= scanner.nextDouble();
        int tiempo= scanner.nextInt();
        return new Peak(name, altura, pais, distancia, tiempo);
    }
}
