package cat.itb.ivanperez7e4.dam.m03.uf2.practica;

import java.util.Scanner;

public class RecursiveFactorial {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int size = scanner.nextInt();
        int[] results = new int [size];

        int userInput = scanner.nextInt();
        int result = recursive(userInput);

        for (int i=0; i<size-1;) {
            results[i] = result;
            ++i;
            userInput = scanner.nextInt();
            result = recursive(userInput);
            results[i] = result;
        }
        for (int i=0;i<size;++i)
            System.out.println(results[i]);
    }

    private static int recursive(int userInput) {
        if (userInput == 0) {
            return userInput + 1;
        } else return userInput * recursive(userInput - 1);
    }

}

