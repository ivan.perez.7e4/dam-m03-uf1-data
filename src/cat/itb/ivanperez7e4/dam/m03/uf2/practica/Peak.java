package cat.itb.ivanperez7e4.dam.m03.uf2.practica;

public class Peak {
    String name;
    int altura;
    String pais;
    double distancia;
    int tiempo;

    public String getName() { return name; }

    public int getAltura() {
        return altura;
    }

    public String getPais() {
        return pais;
    }

    public double getDistancia() {
        return distancia;
    }

    public int getTiempo() {
        return tiempo;
    }

    @Override

    public String toString() {
        return String.format("%s - %s (%dm) - Temps: %s", name, pais, altura, converterMins());
    }


    public String converterMins() {
        int horas = (tiempo / 3600);
        int minutos = ((tiempo-horas * 3600)/60);
        int segundos = tiempo-(horas * 3600+minutos*60);
        return minutos + ":" + segundos;
    }


    public Peak(String name, int altura, String pais, double distancia, int tiempo) {
        this.name = name;
        this.altura = altura;
        this.pais = pais;
        this.distancia = distancia;
        this.tiempo = tiempo;
    }
    public double printAvg() {
        double time = getTiempo();
        double km = getDistancia();
        return (km/1000)/(time/60);
    }

}
