package cat.itb.ivanperez7e4.dam.m03.uf2.dataclasses;

public class NotesParcials {
    String name;
    double notaExercicis;
    double notaExamen;
    double notaProjecte;

    public NotesParcials(String name, double notaExercicis, double notaExamen, double notaProjecte) {
        this.name = name;
        this.notaExercicis = notaExercicis;
        this.notaExamen = notaExamen;
        this.notaProjecte = notaProjecte;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getNotaExercicis() {
        return notaExercicis;
    }

    public void setNotaExercicis(double notaExercicis) {
        this.notaExercicis = notaExercicis;
    }

    public double getNotaExamen() {
        return notaExamen;
    }

    public void setNotaExamen(double notaExamen) {
        this.notaExamen = notaExamen;
    }

    public double getNotaProjecte() {
        return notaProjecte;
    }

    public void setNotaProjecte(double notaProjecte) {
        this.notaProjecte = notaProjecte;
    }




}
