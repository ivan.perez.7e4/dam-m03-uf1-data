package cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CheapestPrice {
    public static void main(String[] args) {
    Scanner scanner = new Scanner(System.in);
    List<Integer> List = IntegerLists.readIntegerList(scanner);
    int precio = IntegerLists.min(List);
        System.out.printf("El producte més econòmic val: %d€", precio);
}
}
