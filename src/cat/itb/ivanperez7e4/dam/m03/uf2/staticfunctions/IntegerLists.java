package cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class IntegerLists {

    public static List<Integer> readIntegerList(Scanner scanner){
        List<Integer> List = new ArrayList<Integer>();
        int userInput = scanner.nextInt();
        while (userInput != -1){
           List.add(userInput);
            userInput = scanner.nextInt();
        }
        return List;
    }

    public static int min(List<Integer> list){
        int minValue = list.get(0);
        for (int i = 0; i < list.size(); ++i){
            if (list.get(i) < minValue){
                minValue = list.get(i);
            }
        }
        return minValue;
    }

    public static int max(List<Integer> list){
        int maxValue = list.get(0);
        for (int i = 0; i < list.size(); ++i){
            if (list.get(i) > maxValue){
                maxValue = list.get(i);
            }
        }
        return maxValue;
    }

    public static double avg(List<Integer> list){
        double media = 0;
        for (int i = 0; i < list.size();++i){
            media+= list.get(i);
        }
        double avgValue = media / list.size();
        return avgValue;
    }

    public static int total(List<Integer> list){
        int total = 0;
        for (int i = 0; i < list.size();++i){
            total+= list.get(i);
        }
        return total;
    }

    public static List<Double> growthRates(List<Integer> dailyCases){
        List<Double> growths = new ArrayList<>();
        for(int i=0; i<dailyCases.size()-1; ++i){
            double growth = ((double) dailyCases.get(i+1)-dailyCases.get(i))/dailyCases.get(i);
            growths.add(growth);
        }
        return growths;
    }

    public static int dif(List<Integer> list) {
        int max = max(list);
        int min = min(list);
        int result = max-min;
        return result;
    }
    public static List<Integer> readFromPath(Path path) throws IOException {
        List<Integer> list = new ArrayList<>();
        Scanner filesSanner = new Scanner(path);
        while(filesSanner.hasNext()){
            int speed = filesSanner.nextInt();
            list.add(speed);
        }
        return list;
    }

}



