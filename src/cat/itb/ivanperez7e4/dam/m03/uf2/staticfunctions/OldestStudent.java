package cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class OldestStudent {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> List = IntegerLists.readIntegerList(scanner);
        int años = IntegerLists.max(List);
        System.out.printf("L'alumne més gran té %d anys", años);
    }

}
