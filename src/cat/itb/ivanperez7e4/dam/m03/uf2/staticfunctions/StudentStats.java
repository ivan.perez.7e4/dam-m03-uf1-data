package cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class StudentStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> List = IntegerLists.readIntegerList(scanner);

        int min = IntegerLists.min(List);
        System.out.println("Nota mínima: "+min);

        int max = IntegerLists.max(List);
        System.out.println("Nota màxima: "+max);

        double media = IntegerLists.avg(List);
        System.out.println("Nota mitjana: "+media);
    }
}
