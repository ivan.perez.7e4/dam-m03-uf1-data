package cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class AvgTemperature {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> List = IntegerLists.readIntegerList(scanner);
        double media = IntegerLists.avg(List);
        System.out.printf("Ha fet %.1f graus de mitjana", media);
    }
}