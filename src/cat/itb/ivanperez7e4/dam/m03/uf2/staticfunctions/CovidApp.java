package cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions;

import java.util.List;
import java.util.Scanner;

public class CovidApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> List = IntegerLists.readIntegerList(scanner);

        int total = IntegerLists.total(List);
        double media = IntegerLists.avg(List);

        List<Double> covid = IntegerLists.growthRates(List);
        double crecimiento = covid.get(covid.size()-1);

        System.out.printf("Hi ha hagut %d casos en total, amb una mitjana de %.2f per dia. \n L'útlim creixement és de %.2f", total, media, crecimiento);

    }
}
