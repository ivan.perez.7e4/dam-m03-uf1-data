package cat.itb.ivanperez7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BuyList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<ShoppingItem> items = readShoppingItems(scanner);

        System.out.println(items);

        printShoppingDetails(items);
    }

    private static void printShoppingDetails(List<ShoppingItem> items) {
        System.out.println("-------- Compra --------");
        printShoppingList(items);
        System.out.println("-------------------------");
        printResumee(items);
        System.out.println("-------------------------");
    }

    private static void printResumee(List<ShoppingItem> items) {
        double total = getTotalAmount(items);
        System.out.printf("Total: %.2f€%n", total);
    }

    private static double getTotalAmount(List<ShoppingItem> items) {
        double total = 0;
        for(ShoppingItem item:items){
            total+= item.getTotal();
        }
        return total;
    }

    private static void printShoppingList(List<ShoppingItem> items) {
        for(ShoppingItem item: items){
            printItem(item);
        }
    }

    private static void printItem(ShoppingItem item) {
        System.out.println(item);
    }

    /**
     * Reads a shopping list form the terminal
     *
     * example:
     * 4
     * 1 Macarrons 2.35
     * 8 Peres 0.35
     * 1 Llet 1.32
     * 2 Galetes 1.23
     *
     * @param scanner reads from
     * @return shopping list
     */
    private static List<ShoppingItem> readShoppingItems(Scanner scanner) {
        List<ShoppingItem> shoppingList = new ArrayList<>();
        int size = scanner.nextInt();
        for(int i=0; i<size; ++i){
            ShoppingItem shoppingItem = readShoppingItem(scanner);
            shoppingList.add(shoppingItem);
        }
        return shoppingList;
    }

    private static ShoppingItem readShoppingItem(Scanner scanner) {
        // 1 Macarrons 2.35
        int amount = scanner.nextInt();
        String name = scanner.next();
        double price = scanner.nextDouble();
        return new ShoppingItem(amount, name, price);
    }
}

