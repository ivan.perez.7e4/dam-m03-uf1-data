package cat.itb.ivanperez7e4.dam.m03.uf2.classfun;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RectangleSize {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        List<Rectangle> rectangles = readRectangles(scanner);
        printRectangles(rectangles);

    }

    public static void printRectangles(List<Rectangle> rectangles) {
        for (Rectangle rectangle : rectangles) {
            printRectangle(rectangle);
        }
    }


    private static void printRectangle(Rectangle rectangle) {
        double height = rectangle.getHeight();
        double widht = rectangle.getWidth();
        double area = rectangle.getArea();
        System.out.printf("Un rectangle de %.2f x %.2f té %.2f d'area.%n", height, widht,area);
    }

    public static List<Rectangle> readRectangles(Scanner scanner) {
        int size = scanner.nextInt();
        List<Rectangle> rectangles = new ArrayList<>();
        for (int i = 0; i <size ; i++) {
            Rectangle rectangle = readRectangle(scanner);
            rectangles.add(rectangle);
        }
        return rectangles;
    }

    private static Rectangle readRectangle(Scanner scanner) {
        double width = scanner.nextDouble();
        double height = scanner.nextDouble();
        return new Rectangle(width,height);

    }

}
