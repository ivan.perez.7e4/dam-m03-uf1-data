package cat.itb.ivanperez7e4.dam.m03.uf2.classfun;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class MostDistantAge {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> age = IntegerLists.readIntegerList(scanner);
        int diferencia = IntegerLists.dif(age);
        System.out.println(diferencia);
    }
}
