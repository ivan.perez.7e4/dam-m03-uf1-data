package cat.itb.ivanperez7e4.dam.m03.uf2.classfun;

public class Book {
    String titulo;
    String autor;
    int paginas;

    public String getTitulo() {
        return titulo;
    }

    public String getAutor() {
        return autor;
    }

    public int getPaginas() {
        return paginas;
    }

    public Book(String titulo, String autor, int paginas) {
        this.titulo = titulo;
        this.autor = autor;
        this.paginas = paginas;
    }


}
