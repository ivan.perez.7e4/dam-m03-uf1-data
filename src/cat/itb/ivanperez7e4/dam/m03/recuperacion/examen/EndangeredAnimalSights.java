package cat.itb.ivanperez7e4.dam.m03.recuperacion.examen;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class EndangeredAnimalSights {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Animal> list = readAnimals(scanner);
        List<Avistaments> listAvistaments = readAvistaments(scanner, list);
        System.out.println("--- Avistaments ---");
        printAvistaments(listAvistaments);
        System.out.println("--- Resum ---");
        printAvistaments(printResum(list, listAvistaments));
    }

    private static List<Avistaments> printResum(List<Animal> list, List<Avistaments> listAvistaments) {
        List<Avistaments> list1 = new ArrayList<>();
        for (Animal animal : list){
            int total = 0;
            for (Avistaments avistament : listAvistaments){
                if (avistament.getAnimal() == animal){
                    total += avistament.getAvistaments();
                }
            }
            list1.add(new Avistaments(animal, total));
        }
        return list1;
    }

    private static void printAvistaments(List<Avistaments> listAvistaments) {
        for (Avistaments avistaments : listAvistaments){
            System.out.printf("%s - %s: %d\n", avistaments.getAnimal().getNomCientific(), avistaments.getAnimal().getNomComu(), avistaments.getAvistaments());
        }
    }

    private static List<Avistaments> readAvistaments(Scanner scanner, List<Animal> list) {
        List<Avistaments> list1 = new ArrayList<>();
        int count = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < count; i++) {
            Avistaments avistament = readAvistament(scanner, list);
            list1.add(avistament);
        }
        return list1;
    }

    private static Avistaments readAvistament(Scanner scanner, List<Animal> list) {
        int posicion = scanner.nextInt();
        int avistaments = scanner.nextInt();
        Animal animal = null;
        for (int i = 0; i < list.size(); i++) {
            if (posicion==i){
                animal = list.get(i);
            }
        }
        return new Avistaments(animal, avistaments);
    }

    private static List<Animal> readAnimals (Scanner scanner){
            List<Animal> list = new ArrayList<>();
            int count = scanner.nextInt();
            scanner.nextLine();
            for (int i = 0; i < count; i++) {
                Animal animal = readAnimal(scanner);
                list.add(animal);
            }
            return list;
        }

        private static Animal readAnimal (Scanner scanner){
            String nomCientific = scanner.nextLine();
            String nomComu = scanner.nextLine();
            return new Animal(nomCientific, nomComu);
        }
    }

