package cat.itb.ivanperez7e4.dam.m03.recuperacion.examen;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import static cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists.readIntegerList;

public class AirContamination {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = readNewIntegerList(scanner);
        double avg = IntegerLists.avg(list);
        int superior = superior(list, avg);
        System.out.printf("Contaminació mitjana: %.2f\n Dies amb contaminació superior: %d", avg, superior);
    }

    private static List<Integer> readNewIntegerList(Scanner scanner) {
        List<Integer> list = new ArrayList<>();
        int input = scanner.nextInt();
        for (int i = 0; i < input; i++) {
            list.add(input);
            input = scanner.nextInt();
        }
        return list;
    }

    private static int superior(List<Integer> list, double value) {
        int count = 0;
        for (int i = 0; i < list.size() ; i++) {
            if (list.get(i) > value){
                count++;}
        }
        return count;
    }
}
