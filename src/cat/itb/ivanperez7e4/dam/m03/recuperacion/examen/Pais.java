package cat.itb.ivanperez7e4.dam.m03.recuperacion.examen;

public class Pais {
    String nom;
    int casos;
    double gravetat;


    public Pais(String nom, int casos, double gravetat) {
        this.nom = nom;
        this.casos = casos;
        this.gravetat = gravetat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public int getCasos() {
        return casos;
    }

    public void setCasos(int casos) {
        this.casos = casos;
    }

    public double getGravetat() {
        return gravetat;
    }

    public void setGravetat(double gravetat) {
        this.gravetat = gravetat;
    }
    public double getPuntuacio(){return getCasos()*getGravetat();}
}
