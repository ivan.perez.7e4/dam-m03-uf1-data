package cat.itb.ivanperez7e4.dam.m03.recuperacion.examen;

import java.util.*;

public class LibertySpeechApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Pais> list = readPaises(scanner);
        System.out.println("---------- Paisos ----------");
        printPaises(list);
        System.out.println("---------- Resum ----------");
        System.out.println("Casos totals: "+totalCases(list));
        System.out.printf("País amb més casos: %s", maxCases(list));
    }

    private static String maxCases(List<Pais> list) {
        Pais maxValue = list.get(0);
        for (Pais pais:list){
            if(pais.getCasos()>maxValue.getCasos()){
                maxValue=pais;
            }
        }
        return maxValue.getNom();
    }


    private static int totalCases(List<Pais> list) {
       int total = 0;
       for (Pais pais: list){
           total+=pais.getCasos();
       }
       return total;
    }

    private static void printPais(Pais pais) {
        String nom = pais.getNom();
        int casos = pais.getCasos();
        double gravetat = pais.getGravetat();
        double puntuacio = pais.getPuntuacio();
        System.out.printf("%s - casos: %d - gravetat: %.1f - puntuacio: %.1f\n", nom, casos, gravetat, puntuacio);
    }

    private static void printPaises(List<Pais> list) {
        for (Pais pais:list) {
            printPais(pais);
        }
    }

    private static List<Pais> readPaises(Scanner scanner) {
        List<Pais> list = new ArrayList<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            Pais pais = readPais(scanner);
            list.add(pais);
        }
        return list;
    }

    private static Pais readPais(Scanner scanner) {
        String nom = scanner.next();
        int casos = scanner.nextInt();
        double gravetat = scanner.nextDouble();
        return new Pais(nom, casos, gravetat);
    }
}
