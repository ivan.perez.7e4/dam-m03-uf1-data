package cat.itb.ivanperez7e4.dam.m03.recuperacion.examen;

public class Avistaments{
    Animal animal;
    int avistaments;

    public Avistaments(Animal animal, int avistaments) {
        this.animal = animal;
        this.avistaments = avistaments;
    }

    public Animal getAnimal() {
        return animal;
    }

    public int getAvistaments() {
        return avistaments;
    }
}
