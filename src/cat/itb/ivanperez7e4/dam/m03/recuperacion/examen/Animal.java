package cat.itb.ivanperez7e4.dam.m03.recuperacion.examen;

public class Animal {
    String nomCientific;
    String nomComu;

    public Animal(String nomCientific, String nomComu) {
        this.nomCientific = nomCientific;
        this.nomComu = nomComu;
    }

    public String getNomCientific() {
        return nomCientific;
    }

    public void setNomCientific(String nomCientific) {
        this.nomCientific = nomCientific;
    }

    public String getNomComu() {
        return nomComu;
    }

    public void setNomComu(String nomComu) {
        this.nomComu = nomComu;
    }
}
