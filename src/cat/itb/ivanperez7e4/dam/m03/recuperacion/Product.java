package cat.itb.ivanperez7e4.dam.m03.recuperacion;

public class Product {
    int cantidad;
    String nom;
    double precio;

    public Product(int cantidad, String nom, double precio) {
        this.cantidad = cantidad;
        this.nom = nom;
        this.precio = precio;
    }

    public int getCantidad() {
        return cantidad;
    }

    public void setCantidad(int cantidad) {
        this.cantidad = cantidad;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
    public double getTotal(){return getCantidad()*getPrecio();}

}
