package cat.itb.ivanperez7e4.dam.m03.recuperacion;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BuyList {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Product> list = readProducts(scanner);
        System.out.println("-------- Compra --------");
        printProducts(list);
        System.out.println("-------------------------");
        System.out.println("Total: "+printTotal(list));
        System.out.println("-------------------------");
    }

    private static double printTotal(List<Product> list) {
        double total = 0;
        for (Product product : list) {
            total+= product.getTotal();
        }
        return total;
    }

    private static void printProducts(List<Product> list) {
        for (Product product : list){
            printProduct(product);
        }
    }

    private static void printProduct(Product product) {
        int cantidad = product.getCantidad();
        String nom = product.getNom();
        double precio = product.getPrecio();
        double total = product.getTotal();
        System.out.printf("%d %s (%.2f€) - %.2f€ \n", cantidad, nom, precio, total);
    }

    private static List<Product> readProducts(Scanner scanner) {
        List<Product> list = new ArrayList<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count ; i++) {
            Product product = readProduct(scanner);
            list.add(product);
        }
        return list;
    }

    private static Product readProduct(Scanner scanner) {
        int cantidad = scanner.nextInt();
        String nom = scanner.next();
        double precio = scanner.nextDouble();
        return new Product(cantidad, nom, precio);
    }
}
