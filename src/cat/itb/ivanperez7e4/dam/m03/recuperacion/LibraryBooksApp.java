package cat.itb.ivanperez7e4.dam.m03.recuperacion;

import java.util.*;

public class LibraryBooksApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Book> list = readBooks(scanner);
        System.out.println("Llibres\n" +
                "------------");
        printBooks(list);
        System.out.println("------------");
        System.out.printf("Total: %s llibres\n", totalBook(list));
        System.out.println("Llibre més curt: "+mesCurt(list));
        System.out.println("Llibre més llarg: "+mesLlarg(list));

    }

    private static Book mesLlarg(List<Book> list) {
        Book maxValue = list.get(0);
        for (Book book:list){
            if(book.getnPages()>maxValue.getnPages()){
                maxValue=book;
            }
        }
        return maxValue;
    }

    private static Book mesCurt(List<Book> list) {
        Book minValue = list.get(0);
        for (Book book:list){
            if(book.getnPages()<minValue.getnPages()){
                minValue=book;
            }
        }
        return minValue;
    }

    private static Object totalBook(List<Book> list) {
        return list.size();
    }


    private static void printBooks(List<Book> list) {
        for (Book book: list){
            System.out.println(book);
        }
    }


    private static List<Book> readBooks(Scanner scanner){
            List<Book> list = new ArrayList<>();
            int count = scanner.nextInt();
            for (int i = 0; i < count; i++) {
                Book book = readBook(scanner);
                list.add(book);
            }
            return list;
        }

        private static Book readBook(Scanner scanner){
            String titol = scanner.next();
            String autor = scanner.next();
            scanner.nextLine();
            int nPages = scanner.nextInt();
            return new Book(titol, autor, nPages);
        }

    }
