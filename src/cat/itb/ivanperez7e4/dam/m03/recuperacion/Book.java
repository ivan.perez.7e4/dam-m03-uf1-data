package cat.itb.ivanperez7e4.dam.m03.recuperacion;

public class Book {
    String titol;
    String autor;
    int nPages;

    public Book(String titol, String autor, int nPages) {
        this.titol = titol;
        this.autor = autor;
        this.nPages = nPages;
    }

    public String getTitol() {
        return titol;
    }

    public void setTitol(String titol) {
        this.titol = titol;
    }

    public String getAutor() {
        return autor;
    }

    public void setAutor(String autor) {
        this.autor = autor;
    }

    public int getnPages() {
        return nPages;
    }

    public void setnPages(int nPages) {
        this.nPages = nPages;
    }

    @Override
    public String toString() {
        return titol +" - "+autor+" - "+nPages+" pàgines";
    }
}
