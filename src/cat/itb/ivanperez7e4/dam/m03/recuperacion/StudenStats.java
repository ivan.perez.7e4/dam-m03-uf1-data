package cat.itb.ivanperez7e4.dam.m03.recuperacion;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.util.List;
import java.util.Scanner;

public class StudenStats {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Integer> list = IntegerLists.readIntegerList(scanner);
        int max = IntegerLists.max(list);
        int min = IntegerLists.min(list);
        double avg = IntegerLists.avg(list);

        System.out.println("Nota mínima: " +min+
                "\nNota màxima: " +max+
                "\nNota mitjana: "+avg);

    }
}
