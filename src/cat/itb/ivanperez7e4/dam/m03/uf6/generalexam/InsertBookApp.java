package cat.itb.ivanperez7e4.dam.m03.uf6.generalexam;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Scanner;

public class InsertBookApp {
    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        LibraryDatabase database = new LibraryDatabase();
        BookDao bookDao = new BookDao(database);

        try(Connection connection = database.connect()) {

            System.out.println("Introduce los datos del libro");
            String title = scanner.next();
            String author = scanner.next();
            String isbn = scanner.next();
            int year = scanner.nextInt();
            int pages = scanner.nextInt();
            bookDao.insertBook(new Book(title, author, isbn, year, pages));
            System.out.println("## Book inserted");
        }
    }
}
