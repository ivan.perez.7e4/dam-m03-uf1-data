package cat.itb.ivanperez7e4.dam.m03.uf6.generalexam;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class ListBooksByYearApp {

    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        LibraryDatabase database = new LibraryDatabase();
        BookDao bookDao = new BookDao(database);
        // connect to DB
        try(Connection connection = database.connect()) {
            // preguntar usuari año
            System.out.println("Introduce el año");
            int year = scanner.nextInt();
            // preguntar DB lista de libros
            List<Book> list = bookDao.inputYearShowBook(year);
            // printar libros
            list.forEach(System.out::println);
        }
    }
}

