package cat.itb.ivanperez7e4.dam.m03.uf6.generalexam;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;
import java.util.Scanner;

public class LongestBookApp {
    public static void main(String[] args) throws SQLException {
        Scanner scanner = new Scanner(System.in);
        LibraryDatabase database = new LibraryDatabase();
        BookDao bookDao = new BookDao(database);
        try(Connection connection = database.connect()) {
            List<Book> list = bookDao.longestBook();
            System.out.println("## Longest Book \n"+list);
        }
    }
}
