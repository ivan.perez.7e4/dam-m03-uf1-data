package cat.itb.ivanperez7e4.dam.m03.uf6.generalexam;



import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BookDao {
    LibraryDatabase database;

    public BookDao(LibraryDatabase database) {
        this.database = database;
    }

    public Connection getConnection(){
        return database.getConnection();
    }

    private List<Book> toBookList(ResultSet resultat) throws SQLException {
        List<Book> books = new ArrayList<>();
        while (resultat.next()) {
            String title = resultat.getString("title");
            String author = resultat.getString("author");
            String isbn = resultat.getString("isbn");
            int year = resultat.getInt("year");
            int pages = resultat.getInt("pages");
            Book book = new Book(title, author, isbn, year, pages);
            books.add(book);
        }
        return books;
    }

    public List<Book> inputYearShowBook(int year) throws SQLException {
        String query = "SELECT * FROM book WHERE year = ?";
        PreparedStatement insertStatement = getConnection().prepareStatement(query);
        insertStatement.setInt(1, year);
        ResultSet resultSet = insertStatement.executeQuery();
        List<Book> resultat = toBookList(resultSet);

        return resultat;
    }

    public void insertBook(Book book) throws SQLException {
        String query = "insert into book values(?, ?, ?, ?, ?) ";
        PreparedStatement insertStatement = getConnection().prepareStatement(query);
        insertStatement.setString(1, book.title);
        insertStatement.setString(2, book.author);
        insertStatement.setString(3, book.isbn);
        insertStatement.setInt(4, book.year);
        insertStatement.setInt(5, book.pages);
        insertStatement.execute();
    }

    public List<Book> longestBook() throws SQLException {
        String query = "select * from book where pages = (select max(pages) from book)";
        PreparedStatement insertStatement = getConnection().prepareStatement(query);
        ResultSet resultSet = insertStatement.executeQuery();
        List<Book> resultat = toBookList(resultSet);

        return resultat;
    }
}
