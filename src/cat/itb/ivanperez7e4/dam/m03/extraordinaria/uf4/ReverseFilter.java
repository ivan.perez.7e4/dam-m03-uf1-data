package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

public class ReverseFilter extends Filter{
    public ReverseFilter(String text) {
        super(text);
    }
    @Override
    public void filterText(String text) {
        StringBuilder input = new StringBuilder();
        input.append(text);
        input.reverse();
        System.out.println(input);
    }
}
