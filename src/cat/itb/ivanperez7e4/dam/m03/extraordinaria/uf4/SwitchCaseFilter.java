package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

public class SwitchCaseFilter extends Filter{
    public SwitchCaseFilter(String text) {
        super(text);
    }

    @Override
    public void filterText(String text) {
        text = text.toUpperCase();
        char[] array = text.toCharArray();
        for(int i=0; i<array.length; i=i+2){
            array[i] = Character.toLowerCase(array[i]);
        }
        System.out.println(new String(array));
    }
}
