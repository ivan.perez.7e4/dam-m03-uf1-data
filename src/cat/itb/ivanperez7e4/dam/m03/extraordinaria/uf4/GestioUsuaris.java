package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

public class GestioUsuaris {
    public static void main(String[] args) {
        User user = new User("Dani", "Carrasco", "Morera", "04/09/1986");
        User user2 = new User("Laura", "Ivars", "Perelló", "17/05/1996");
        Student student = new Student("Aitana", "Sanchis", "Marí", "20/11/2001", 2020);
        System.out.println(user);
        System.out.println(user2);
        System.out.println(student);

    }
}
