package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

public class Student extends User{
    int dataMatriculacio;

    public Student(String nom, String primerCognom, String segonCognom, String dataNaixement, int dataMatriculacio) {
        super(nom, primerCognom, segonCognom, dataNaixement);
        this.dataMatriculacio = dataMatriculacio;
    }

    @Override
    public String toString() {
        return "Nom: "+nom+
                "\nCognoms: "+primerCognom+" "+segonCognom+
                "\nData de naixement: "+dataNaixement+
                "\nData de matriculació: "+dataMatriculacio+
                "\nCorreu electrònic: "+nom+"."+primerCognom+"."+Integer.toHexString((dataMatriculacio))+"@itb.cat\n";
    }
}
