package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

public class User {
    String nom;
    String primerCognom;
    String segonCognom;
    String dataNaixement;

    public User(String nom, String primerCognom, String segonCognom, String dataNaixement) {
        this.nom = nom;
        this.primerCognom = primerCognom;
        this.segonCognom = segonCognom;
        this.dataNaixement = dataNaixement;
    }

    public String getNom() {
        return nom;
    }

    public String getPrimerCognom() {
        return primerCognom;
    }

    public String getSegonCognom() {
        return segonCognom;
    }

    public String getDataNaixement() {
        return dataNaixement;
    }

    @Override
    public String toString() {
        return "Nom: "+nom+
                "\nCognoms: "+primerCognom+" "+segonCognom+
                "\nData de naixement: "+dataNaixement+
                "\nCorreu electrònic: "+nom+"."+primerCognom+"@itb.cat\n";
    }
}
