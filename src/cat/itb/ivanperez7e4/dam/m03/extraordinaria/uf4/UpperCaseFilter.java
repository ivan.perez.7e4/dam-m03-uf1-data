package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

public class UpperCaseFilter extends Filter{

    public UpperCaseFilter(String text) {
        super(text);
    }

    @Override
    public void filterText(String text) {
        System.out.println(text.toUpperCase());
    }
}
