package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf4;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FiltreText {
    public static void main(String[] args) {
        List<Filter> list = new ArrayList<>();
        Scanner scanner = new Scanner(System.in);
        String text = scanner.nextLine();
        list.add(new UpperCaseFilter(text));
        list.add(new SwitchCaseFilter(text));
        list.add(new ReverseFilter(text));
        printList(list);
    }

    private static void printList(List<Filter> list) {
        for (Filter filter : list){
            filter.filterText(filter.text);
        }
    }
}
