package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf2;

public class Animal {
    String codi;
    String nom;
    int apats;
    int quantitat;

    public Animal(String codi, String nom, int apats, int quantitat) {
        this.codi = codi;
        this.nom = nom;
        this.apats = apats;
        this.quantitat = quantitat;
    }

    public String getCodi() {
        return codi;
    }

    public String getNom() {
        return nom;
    }

    public int getApats() {
        return apats;
    }

    public int getQuantitat() {
        return quantitat;
    }
    public int getTotal(){return apats*quantitat;}

    @Override
    public String toString() {
        return codi+"-"+nom+": "+apats+" àpats, "+quantitat+"g/apat, total "+getTotal()+"g";
    }
}
