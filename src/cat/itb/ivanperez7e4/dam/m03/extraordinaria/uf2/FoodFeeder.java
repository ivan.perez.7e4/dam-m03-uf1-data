package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class FoodFeeder {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Animal> list = readAnimals(scanner);
        printTotal(list);
    }

    private static void printTotal(List<Animal> list) {
        printAnimals(list);
        printResum(list);
    }

    private static void printResum(List<Animal> list) {
        System.out.println("---------- Resum ----------");
        System.out.println("Total: "+ getTotal(list));
        System.out.println("Animal més golós: "+getMesGolos(list));
    }

    private static String getMesGolos(List<Animal> list) {
        Animal mesGolos = list.get(0);
        for (Animal animal : list) {
            if (animal.getTotal()>mesGolos.getTotal()){
                mesGolos=animal;
            }
        }
        return mesGolos.codi+"-"+mesGolos.getNom();
    }

    private static int getTotal(List<Animal> list) {
        int total = 0;
        for (Animal animal : list){
            total+=animal.getTotal();
        }
        return total;
    }

    private static void printAnimals(List<Animal> list) {
        System.out.println("---------- Animals ----------");
        for (Animal animal : list){
            System.out.println(animal);
        }
    }

    private static List<Animal> readAnimals(Scanner scanner) {
        List<Animal> list = new ArrayList<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            Animal animal = readAnimal(scanner);
            list.add(animal);
        }
        return list;
    }

    private static Animal readAnimal(Scanner scanner) {
        String codi = scanner.next();
        String nom = scanner.next();
        int apats = scanner.nextInt();
        int quantitat = scanner.nextInt();
        scanner.nextLine();
        return new Animal(codi, nom, apats, quantitat);
    }
}
