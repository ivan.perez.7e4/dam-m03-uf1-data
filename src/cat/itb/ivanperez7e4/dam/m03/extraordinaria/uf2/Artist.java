package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf2;

public class Artist {
    String nom;
    String pais;

    public Artist(String nom, String pais) {
        this.nom = nom;
        this.pais = pais;
    }

    public String getNom() {
        return nom;
    }

    public String getPais() {
        return pais;
    }

    @Override
    public String toString() {
        return nom+"("+pais+")";
    }
}
