package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf2;

public class Album {
    Artist artista;
    String nom;
    int any;

    public Album(Artist artista, String nom, int any) {
        this.artista = artista;
        this.nom = nom;
        this.any = any;
    }

    public Artist getArtista() {
        return artista;
    }

    public String getNom() {
        return nom;
    }

    public int getAny() {
        return any;
    }
}
