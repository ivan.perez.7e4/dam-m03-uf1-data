package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class DivisibleCount {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int divisibilitat = scanner.nextInt();
        List<Integer> list = readList(scanner);
        printResult(divisibilitat, list);
    }

    private static void printResult(int divisibilitat, List<Integer> list) {
        int count =0;
        for (int i = 0; i < divisibilitat; i++) {
            divisble(count, i, list);
            System.out.printf("%d: %d\n", divisibilitat, count);
            divisibilitat--;
        }
    }
    private static int divisble(int count, int i, List<Integer> list) {
        for (Integer integer : list) {
            if (i % integer == 0) {
                count++;
            }
        }
        return count;
    }

    private static List<Integer> readList(Scanner scanner) {
        List<Integer> list = new ArrayList<Integer>();
        int userInput = scanner.nextInt();
        while (userInput != -1){
            list.add(userInput);
            userInput = scanner.nextInt();
        }
        return list;
    }
}
