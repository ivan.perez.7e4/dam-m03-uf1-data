package cat.itb.ivanperez7e4.dam.m03.extraordinaria.uf2;


import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ArtistsAndAlbums {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<Artist> artistList = readArtistas(scanner);
        List<Album> albumList = readAlbums(scanner, artistList);
        printFinalList(albumList);
    }

    private static void printFinalList(List<Album> albumList) {
        System.out.println("#### Albums\n");
        for (Album album : albumList){
            System.out.printf("%s - %s - %d\n",album.getArtista(), album.getNom(), album.getAny());
        }
    }

    private static List<Album> readAlbums(Scanner scanner, List<Artist> list) {
        List<Album> list1 = new ArrayList<>();
        int count = scanner.nextInt();
        scanner.nextLine();
        for (int i = 0; i < count; i++) {
            Album album = readAlbum(scanner, list);
            list1.add(album);
        }
        return list1;
    }

    private static Album readAlbum(Scanner scanner, List<Artist> list) {
        int posicion = scanner.nextInt();
        scanner.nextLine();
        String nom = scanner.nextLine();
        int any = scanner.nextInt();
        Artist artist = null;
        for (int i = 0; i < list.size(); i++) {
            if (posicion==i){
                artist = list.get(i);
            }
        }
        return new Album(artist, nom, any);
    }


    private static List<Artist> readArtistas(Scanner scanner) {
        List<Artist> list = new ArrayList<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            Artist artist = readArtist(scanner);
            list.add(artist);
        }
        return list;
    }

    private static Artist readArtist(Scanner scanner) {
        String nom = scanner.next();
        String pais = scanner.next();
        return new Artist(nom, pais);
    }
}
