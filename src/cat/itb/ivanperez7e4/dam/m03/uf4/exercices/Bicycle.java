package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class Bicycle {
    public static void main(String[] args) {

        BicycleBrand bicycleBrand = new BicycleBrand("dsfn", "España");
        BicycleModel bicycleModel = new BicycleModel("gfjhgfh", bicycleBrand, 8);
        BicycleModel bicycleModel2 = new BicycleModel("nfkdjsk", bicycleBrand, 2);
        ScooterModel scooterModel = new ScooterModel("s562",bicycleBrand,45.3);

        System.out.println(bicycleModel);
        System.out.println(bicycleModel2);
        System.out.println(scooterModel);
    }
}