package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class Team {
    String team;
    String crit;

    public Team(String team, String crit) {
        this.team = team;
        this.crit = crit;
    }

    public void shoutMotto(){
        System.out.println(crit);
    }
}
