package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class MechanicalArm {
    double openAngle; // degrees
    boolean turnedOn;
    double altitude; // cm

    public MechanicalArm() {
    }

    public void setTurnedOn(boolean turnedOn) {
        this.turnedOn = turnedOn;
    }

    public void updateOpenAngle(double angleChange){
        if(turnedOn) {
            openAngle = Math.min(360,Math.max(0,openAngle+angleChange));
        }
    }

    public void updateAltitude(double altitudeChange){
        if(turnedOn)
            this.altitude=Math.max(0,altitude+altitudeChange);
    }

    @Override
    public String toString() {
        return "MechanicalArm{" +
                "openAngle=" + openAngle +
                ", turnedOn=" + turnedOn +
                ", altitude=" + altitude +
                '}';
    }

}
