package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class BicycleBrand {
    String nom;
    String pais;

    public BicycleBrand(String nom, String pais) {
        this.nom = nom;
        this.pais = pais;
    }

    @Override
    public String toString() {
        return "BicycleBrand{" +
                "nom='" + nom + '\'' +
                ", pais='" + pais + '\'' +
                '}';
    }
}
