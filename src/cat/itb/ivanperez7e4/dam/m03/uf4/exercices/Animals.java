package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.time.LocalDateTime;

public class Animals extends EsserViu{
    Boolean funcioCerebrals;
    Boolean funcionaCor;

    public Animals(String nom, LocalDateTime date, Boolean funcioCerebrals, Boolean funcionaCor) {
        super(nom, date);
        this.funcioCerebrals = funcioCerebrals;
        this.funcionaCor = funcionaCor;
    }

    @Override
    public String toString() {
        return "Animals{" +
                "funcioCerebrals=" + funcioCerebrals +
                ", funcionaCor=" + funcionaCor +
                ", nom='" + nom + '\'' +
                ", date=" + date+
                ", Is alive=" + isAlive()+'}';
    }

    @Override
    public boolean isAlive() {
        if (funcioCerebrals && funcionaCor) {
            return true;
        } else {
            return false;
        }
    }
}
