package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class MovingMechanicalArm extends MechanicalArm {
    double position;

    public void move(double movement){
        if(turnedOn)
            position = Math.max(0,position+movement);
    }

    @Override
    public String toString() {
        return "MovingMechanicalArm{" +
                "openAngle=" + openAngle +
                ", turnedOn=" + turnedOn +
                ", altitude=" + altitude +
                ", position=" + position +
                '}';
    }

}
