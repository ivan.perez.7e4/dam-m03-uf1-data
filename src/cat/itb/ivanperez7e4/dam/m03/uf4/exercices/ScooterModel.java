package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class ScooterModel extends Vehicle{
    double power;

    public ScooterModel(String nom, BicycleBrand marca, double power) {
        super(nom, marca);
        this.power = power;
    }

    @Override
    public String toString() {
        return "ScooterModel{" +
                "power=" + power +
                ", nom='" + nom + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }
}
