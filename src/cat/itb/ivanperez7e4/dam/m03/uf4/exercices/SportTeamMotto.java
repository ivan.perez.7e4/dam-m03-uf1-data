package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.util.ArrayList;
import java.util.List;

public class SportTeamMotto {
    public static void main(String[] args) {
        List<Team> list = new ArrayList<>();
        Team team1 = new Basquet("Mosques", "Bzzzanyarem");
        Team team2 = new VoleiTeam("Dragons", "Grooarg", "Verd");
        Team team3 = new Golf("Abelles", "Piquem Fort", "Marta Ahuja");
        list.add(team1);
        list.add(team2);
        list.add(team3);
        shoutMottos(list);
    }
    public static void shoutMottos(List<Team> teams){
        for(Team team: teams)
            team.shoutMotto();
    }
}
