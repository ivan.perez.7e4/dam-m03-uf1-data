package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.time.LocalDateTime;

public abstract class EsserViu {
    String nom;
    LocalDateTime date;

    public EsserViu(String nom, LocalDateTime date) {
        this.nom = nom;
        this.date = date;
    }

    public abstract boolean isAlive();
}
