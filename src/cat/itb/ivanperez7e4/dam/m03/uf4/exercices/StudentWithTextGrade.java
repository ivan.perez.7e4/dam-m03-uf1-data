package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class StudentWithTextGrade {
    String name;
    EnumNota nota;

    public StudentWithTextGrade(String name, EnumNota nota) {
        this.name = name;
        this.nota = nota;
    }

    @Override
    public String toString() {
        return "StudentWithTextGrade{" +
                "name='" + name + '\'' +
                ", nota=" + nota +
                '}';
    }
}
