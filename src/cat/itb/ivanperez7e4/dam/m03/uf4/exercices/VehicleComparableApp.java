package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VehicleComparableApp {
    public static void main(String[] args) {
        List<Vehicle> list = new ArrayList<>();
        BicycleBrand bicycleBrand = new BicycleBrand("dsfn", "España");
        BicycleModel bicycleModel = new BicycleModel("gfjhgfh", bicycleBrand, 8);
        BicycleModel bicycleModel2 = new BicycleModel("nfkdjsk", bicycleBrand, 2);
        ScooterModel scooterModel = new ScooterModel("s562",bicycleBrand,45.3);
        BicycleModel bicycleModel3 = new BicycleModel("a", bicycleBrand, 2);

        list.add(bicycleModel);
        list.add(bicycleModel2);
        list.add(scooterModel);
        list.add(bicycleModel3);

        Collections.sort(list);
        System.out.println(list);
    }


}
