package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class Golf extends  Team {
    String jugador;

    public Golf(String team, String crit, String jugador) {
        super(team, crit);
        this.jugador = jugador;
    }
    public void shoutMotto(){
        System.out.println(crit + " "+jugador);
    }
}
