package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.util.ArrayList;
import java.util.List;

public class TeamMotto {
    public static void main(String[] args) {
        List<Team> list = new ArrayList<>();
        Team team1 = new Team("Mosques", "Bzzzanyarem");
        Team team2 = new Team("Dragons", "Grooarg");
        Team team3 = new Team("Abelles", "Piquem Fort");
        list.add(team1);
        list.add(team2);
        list.add(team3);
        shoutMottos(list);

    }
    public static void shoutMottos(List<Team> teams){
        for(Team team: teams)
            team.shoutMotto();
    }
}
