package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class VoleiTeam extends Team{
    String color;

    public VoleiTeam(String team, String crit, String color) {
        super(team, crit);
        this.color = color;
    }

    @Override
    public void shoutMotto(){
        System.out.println(crit+ " "+ color);
    }
}
