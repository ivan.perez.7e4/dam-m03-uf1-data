package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class BicycleModel extends Vehicle{
   int gears;

    public BicycleModel(String nom, BicycleBrand marca, int gears) {
        super(nom, marca);
        this.gears = gears;
    }

    @Override
    public String toString() {
        return "BicycleModel{" +
                "gears=" + gears +
                ", nom='" + nom + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }
}