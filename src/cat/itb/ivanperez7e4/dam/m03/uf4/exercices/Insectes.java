package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.time.LocalDateTime;

public class Insectes extends EsserViu{
    Boolean funcioCerebral;

    public Insectes(String nom, LocalDateTime date, Boolean sistemaCirculatori) {
        super(nom, date);
        this.funcioCerebral = sistemaCirculatori;
    }

    @Override
    public String toString() {
        return "Insectes{" +
                "nom='" + nom + '\'' +
                ", date=" + date +
                ", funcioCerebral=" + funcioCerebral +
                ", Is alive=" + isAlive()+'}';

    }

    @Override
    public boolean isAlive() {
        if (funcioCerebral) {
            return true;
        } else {
            return false;
        }
    }
}
