package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class AliveBeings {
    public static void main(String[] args) {
        List<EsserViu> list = new ArrayList<>();
        EsserViu a1 = new Insectes("Jan", LocalDateTime.now(), true);
        EsserViu a2 = new Animals("Jan", LocalDateTime.now(), false, true);
        list.add(a1);
        list.add(a2);

        isAlive(list);
    }

    public static void isAlive(List<EsserViu> esser){
        for(EsserViu esserViu: esser)
            System.out.println(esserViu);
    }
}
