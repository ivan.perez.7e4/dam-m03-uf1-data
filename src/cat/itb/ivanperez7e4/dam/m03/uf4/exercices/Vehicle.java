package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

public class Vehicle implements Comparable<cat.itb.ivanperez7e4.dam.m03.uf4.exercices.Vehicle> {
    String nom;
    BicycleBrand marca;

    public Vehicle(String nom, BicycleBrand marca) {
        this.nom = nom;
        this.marca = marca;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "nom='" + nom + '\'' +
                ", marca='" + marca + '\'' +
                '}';
    }

    @Override
    public int compareTo(cat.itb.ivanperez7e4.dam.m03.uf4.exercices.Vehicle vehicle) {
        return nom.compareTo(vehicle.nom);
    }
}
