package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.util.ArrayList;
import java.util.List;

public class Student {
    public static void main(String[] args) {
        List<StudentWithTextGrade> list = new ArrayList<>();
        StudentWithTextGrade student1 = new StudentWithTextGrade("Mar",EnumNota.APROVAT);
        StudentWithTextGrade student2 = new StudentWithTextGrade("Joan",EnumNota.EXCELLENT);
        list.add(student1);
        list.add(student2);
        System.out.println(list);
    }
}
