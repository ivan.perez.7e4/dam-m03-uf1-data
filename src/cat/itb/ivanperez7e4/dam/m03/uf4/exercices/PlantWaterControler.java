package cat.itb.ivanperez7e4.dam.m03.uf4.exercices;

import java.util.List;

public class PlantWaterControler {
    PlantWater plantWater;

    public PlantWaterControler(PlantWater plantWater) {
        this.plantWater = plantWater;
    }

    public void waterIfNeeded(){

    double avg = calAvg();
    if (avg<2){
        plantWater.startWatterSystem();
    }
    }

    private double calAvg() {
        List<Double> humidity = plantWater.getHumidityRecord();
        double avg =0;
        for (int i = 0; i < humidity.size(); i++) {
            avg+=humidity.get(i);
        }
        double average = avg / humidity.size();
        return average;
    }
}