package cat.itb.ivanperez7e4.dam.m03.uf4.figures;

import java.io.PrintStream;

public class RectangleFigure extends Figure{
    int width;
    int height;

    public RectangleFigure(String color, int width, int height) {
        super(color);
        this.width = width;
        this.height = height;
    }

    @Override
    public void paintDots(PrintStream printStream){
        for(int i=0; i<height; ++i) {
            for (int j = 0; j < width; ++j) {
                printStream.print("X");
            }
            printStream.println();
        }
        printStream.println();
    }
}
