package cat.itb.ivanperez7e4.dam.m03.uf4.figures;

import java.io.PrintStream;

public abstract class Figure {
    String color;

    public Figure(String color) {
        this.color = color;
    }

    public void changeColor(PrintStream printStream){
        printStream.print(color);
    }

    public void resetColor(PrintStream printStream){
        printStream.print(ConsoleColors.RESET);
    }

    public void paint(PrintStream printStream){
        changeColor(printStream);
        paintDots(printStream);
        resetColor(printStream);
    }

    protected abstract void paintDots(PrintStream printStream);

}
