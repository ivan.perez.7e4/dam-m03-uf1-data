package cat.itb.ivanperez7e4.dam.m03.uf4.figures;

import java.util.ArrayList;
import java.util.List;

public class ListOfFigurePainter {
    public static void main(String[] args) {
        List<Figure> figures = new ArrayList<>();

        figures.add(new RectangleFigure(ConsoleColors.RED, 4, 5));
        figures.add(new LeftPiramidFigure(ConsoleColors.YELLOW, 3));
        figures.add(new RectangleFigure(ConsoleColors.GREEN, 3, 5));

        for(Figure figure: figures){
            figure.paint(System.out);
        }
    }

}
