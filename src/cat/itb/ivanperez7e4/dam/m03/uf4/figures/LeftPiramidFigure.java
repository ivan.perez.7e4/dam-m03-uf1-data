package cat.itb.ivanperez7e4.dam.m03.uf4.figures;

import java.io.PrintStream;

public class LeftPiramidFigure extends Figure{
    int base;

    public LeftPiramidFigure(String color, int base) {
        super(color);
        this.base = base;
    }

    @Override
    public void paintDots(PrintStream printStream){
        for (int i = 0; i < base; i++) {
            for (int j = 0; j <= i; j++) {
                System.out.print("X");
            }
            System.out.println();
        }
        System.out.println();
    }
}
