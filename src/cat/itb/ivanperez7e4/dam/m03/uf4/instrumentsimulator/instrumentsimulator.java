package cat.itb.ivanperez7e4.dam.m03.uf4.instrumentsimulator;

import java.util.ArrayList;
import java.util.List;

public class instrumentsimulator {
    public static void main(String[] args) {
        List<Instrument> instruments = new ArrayList<>();
        instruments.add(new Triangle(5));
        instruments.add(new Tambor("A"));
        instruments.add(new Tambor("O"));
        instruments.add(new Triangle(1));
        instruments.add(new Triangle(5));

        play(instruments);
    }

    private static void play(List<Instrument> instruments) {
        for(Instrument instrument: instruments){
            instrument.makeSound();
        }
    }
}
