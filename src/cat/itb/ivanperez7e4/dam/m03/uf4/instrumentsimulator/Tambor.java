package cat.itb.ivanperez7e4.dam.m03.uf4.instrumentsimulator;

public class Tambor extends Instrument{
    String to;

    public Tambor(String to) {
        this.to = to;
    }

    @Override
    public void makeSound() {
        if (to.equals("A")){
            System.out.println("TAAAM");
        }
        else if (to.equals("O")){
            System.out.println("TOOOM");
        }
        else if (to.equals("U")){
            System.out.println("TUUUM");
        }
    }
}
