package cat.itb.ivanperez7e4.dam.m03.uf4.instrumentsimulator;

public abstract class Instrument {

    public abstract void makeSound();
}
