package cat.itb.ivanperez7e4.dam.m03.uf4.instrumentsimulator;

public class Triangle extends Instrument{
    int resonancia;

    public Triangle(int resonancia) {
        this.resonancia = resonancia;
    }

    @Override
    public void makeSound() {
        if (resonancia==1){
            System.out.println("TINC");
        }
        else if (resonancia==2){
            System.out.println("TIINC");
        }
        else if (resonancia==3){
            System.out.println("TIIINC");
        }
        else if (resonancia==4){
            System.out.println("TIIIINC");
        }
        else if (resonancia==5){
            System.out.println("TIIIIINC");
        }
    }
}
