package cat.itb.ivanperez7e4.dam.m03.uf4.carpentery;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class CarpentryShop {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        List<CarpentryProduct> productList = readProducts(scanner);
        double price = calculateTotalPrice(productList);
        System.out.printf("El preu total és: %.2f€%n", price);
    }

    private static double calculateTotalPrice(List<CarpentryProduct> productList) {
        double price = 0;
        for(CarpentryProduct product: productList){
            price += product.getTotalPrice();
        }
        return price;
    }

    private static List<CarpentryProduct> readProducts(Scanner scanner) {
        List<CarpentryProduct> list = new ArrayList<>();
        int count = scanner.nextInt();
        for (int i = 0; i < count; i++) {
            CarpentryProduct product = readProduct(scanner);
            list.add(product);
        }
        return list;
    }

    private static CarpentryProduct readProduct(Scanner scanner) {
        String type = scanner.next();
        switch (type){
            case "Taulell":
                return readBoard(scanner);
            case "Llistó":
                return readStrip(scanner);
        }
        System.out.println("TIPUS INCORRECTE");
        return readProduct(scanner);
    }

    private static CarpentryProduct readStrip(Scanner scanner) {
        double price = scanner.nextDouble();
        double length = scanner.nextDouble();
        return new Strip(price, length);
    }

    private static CarpentryProduct readBoard(Scanner scanner) {
        double price = scanner.nextDouble();
        double height = scanner.nextDouble();
        double width = scanner.nextDouble();
        return new Board(price, height, width);
    }

}
