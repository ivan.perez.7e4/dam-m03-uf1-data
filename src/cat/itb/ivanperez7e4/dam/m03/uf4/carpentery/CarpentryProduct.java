package cat.itb.ivanperez7e4.dam.m03.uf4.carpentery;

public abstract class CarpentryProduct {
    double unitPrice;

    public CarpentryProduct(double unitPrice) {
        this.unitPrice = unitPrice;
    }

    public double getTotalPrice(){
        return unitPrice* getAmount();
    }

    protected abstract double getAmount();

}
