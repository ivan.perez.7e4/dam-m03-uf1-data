package cat.itb.ivanperez7e4.dam.m03.uf4.carpentery;

public class Board extends CarpentryProduct{
    double width;
    double height;

    public Board(double unitPrice, double width, double height) {
        super(unitPrice);
        this.width = width;
        this.height = height;
    }

    @Override
    protected double getAmount() {
        return width*height;
    }

}
