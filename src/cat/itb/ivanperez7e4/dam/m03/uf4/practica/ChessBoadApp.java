package cat.itb.ivanperez7e4.dam.m03.uf4.practica;


import java.util.Scanner;

public class ChessBoadApp {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        ChessBoard board = new ChessBoard();
        board.paint();
        int pF = scanner.nextInt();

        while(pF != -1){
            int pF2 = scanner.nextInt();
            int nP = scanner.nextInt();
            int nP2 = scanner.nextInt();
            board.move(pF, pF2, nP, nP2);
            board.paint();
            pF = scanner.nextInt();
        }
    }
}
