package cat.itb.ivanperez7e4.dam.m03.uf4.practica;

public class King extends ChessPiece{
    public King(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♚";
    }

    @Override
    public boolean correctMove(int posicionFicha, int posicionFicha2, int nuevaPosicion, int nuevaPosicion2) {
        if ((Math.abs(posicionFicha-nuevaPosicion)<=1) && (Math.abs(posicionFicha2-nuevaPosicion2) <=1)){
            return true;
        }
        return false;
    }

}
