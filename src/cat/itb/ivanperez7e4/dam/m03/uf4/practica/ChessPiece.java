package cat.itb.ivanperez7e4.dam.m03.uf4.practica;


public abstract class ChessPiece {
    boolean white;

    public ChessPiece(boolean white) {
        this.white = white;
    }

    public void paint(){
        String color = white ? "\u001B[31m" : "\u001B[34m";
        String piece = getPieceString();
        System.out.printf("%s%s\u001B[0m", color, piece);
    }

    public abstract String getPieceString();

    public abstract boolean correctMove(int posicionFicha, int posicionFicha2, int nuevaPosicion, int nuevaPosicion2);
}
