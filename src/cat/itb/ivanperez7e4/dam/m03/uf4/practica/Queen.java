package cat.itb.ivanperez7e4.dam.m03.uf4.practica;

public class Queen extends ChessPiece{
    public Queen(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♛";
    }

    @Override
    public boolean correctMove(int posicionFicha, int posicionFicha2, int nuevaPosicion, int nuevaPosicion2) {
        if ((posicionFicha==nuevaPosicion) || (posicionFicha2==nuevaPosicion2) || (Math.abs(posicionFicha-nuevaPosicion) == Math.abs(posicionFicha2-nuevaPosicion2))) {
            return true;
        }
        return false;
    }

}
