package cat.itb.ivanperez7e4.dam.m03.uf4.practica;

public class ChessBoard {
    ChessPiece[][] board;
    public ChessBoard() {
        board = new ChessPiece[8][8];

        board[0][0] = new Rock(false);
        board[0][1] = new Knight(false);
        board[0][2] = new Bishop(false);
        board[0][3] = new King(false);
        board[0][4] = new Queen(false);
        board[0][5] = new Bishop(false);
        board[0][6] = new Knight(false);
        board[0][7] = new Rock(false);

        board[1][0] = new Pawn(false);
        board[1][1] = new Pawn(false);
        board[1][2] = new Pawn(false);
        board[1][3] = new Pawn(false);
        board[1][4] = new Pawn(false);
        board[1][5] = new Pawn(false);
        board[1][6] = new Pawn(false);
        board[1][7] = new Pawn(false);

        board[6][0] = new Pawn(true);
        board[6][1] = new Pawn(true);
        board[6][2] = new Pawn(true);
        board[6][3] = new Pawn(true);
        board[6][4] = new Pawn(true);
        board[6][5] = new Pawn(true);
        board[6][6] = new Pawn(true);
        board[6][7] = new Pawn(true);

        board[7][0] = new Rock(true);
        board[7][1] = new Knight(true);
        board[7][2] = new Bishop(true);
        board[7][3] = new King(true);
        board[7][4] = new Queen(true);
        board[7][5] = new Bishop(true);
        board[7][6] = new Knight(true);
        board[7][7] = new Rock(true);
    }
    public void paint() {
        System.out.println("  0 1 2 3 4 5 6 7");
        int linea = 0;
        for (ChessPiece[] row: board){
            System.out.printf("%d|", linea++);
            for (ChessPiece piece : row){
                if (piece != null) piece.paint();
                else System.out.print(" ");
                System.out.print("|");
            }
            System.out.println();
        }
    }

    public void move(int pF, int pF2, int nP, int nP2) {
        if (board[pF][pF2].correctMove(pF, pF2, nP, nP2)){
            board[nP][nP2]=board[pF][pF2];
            board[pF][pF2]=null;
        }
        else
            System.out.println("Este movimiento no es posible");

    }
}

