package cat.itb.ivanperez7e4.dam.m03.uf4.practica;

public class Knight extends ChessPiece{
    public Knight(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♞";
    }

    @Override
    public boolean correctMove(int posicionFicha, int posicionFicha2, int nuevaPosicion, int nuevaPosicion2) {
       if ((posicionFicha-nuevaPosicion)*(posicionFicha-nuevaPosicion) +(posicionFicha2-nuevaPosicion2)*(posicionFicha2-nuevaPosicion2) == 5){
           return true;
       }
       return false;
    }

}
