package cat.itb.ivanperez7e4.dam.m03.uf4.practica;

public class Pawn extends ChessPiece{
    public Pawn(boolean white) {
        super(white);
    }

    @Override
    public String getPieceString() {
        return "♟";
    }

    @Override
    public boolean correctMove(int posicionFicha, int posicionFicha2, int nuevaPosicion, int nuevaPosicion2) {
        return false;
    }

}
