package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.gameenemies;

public class Troll extends Enemy{
    int resistencia;

    public Troll(String name, Integer life, int resistencia) {
        super(name, life);
        this.resistencia = resistencia;
    }

    @Override
    public void attack(int fuerza) {
        if (fuerza>resistencia){
            int ataqueFinal = fuerza-resistencia;
            life=life-ataqueFinal;
        }
        if (life>0){
            System.out.printf("L'enemic %s té %d punts de vida després d'un atac de força %d\n", name, life, fuerza);
        }
        else System.out.printf("L'enemic %s ja està mort\n", name);
    }
}
