package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.twitter;

public class PollTweet extends Tweet{
    String option;

    public PollTweet(String user, String date, String text, String option) {
        super(user, date, text);
        this.option = option;
    }

    //No me ha salido introducir varias opciones
    //Se me ocurrio hacer PollTweet.option() he ir añadiendo opciones a una lista
    //Y despues printar la lista, pero no me ha dado tiempo
}
