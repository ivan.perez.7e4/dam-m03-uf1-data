package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.twitter;

public class QATweet extends Tweet{
    String answer;

    public QATweet(String user, String date, String text, String answer) {
        super(user, date, text);
        this.answer = answer;
    }

    @Override
    public String toString() {
        return "@"+ user + " · "+ date+ "\n"
                + "Q: "+text +"\n\n"+
                "A: "+answer;

    }
}
