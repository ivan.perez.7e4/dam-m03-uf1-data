package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.twitter;

public class TweetTest {
    public static void main(String[] args) {
        Tweet tweet1 = new Tweet("iamdevloper", "07 de gener","Remember, a few hours of trial and error can save you several minutes of looking at the README");
        System.out.println(tweet1);

        Tweet tweet2 = new Tweet("softcatala", "29 de març", "Avui mateix, #CommonVoiceCAT segueix creixent \uD83D\uDE80: \\n \uD83D\uDDE3️ 856 hores enregistrades\\n✅ 725 de validades.\\nSi encara no has participat, pots fer-ho aquí!");
        System.out.println(tweet2);



        Tweet tweet4 = new QATweet("ProgrammerJokes", "05 d'abril", "what's the object-oriented way to become weathy?", "Inheritance");
        System.out.println(tweet4);
    }
}
