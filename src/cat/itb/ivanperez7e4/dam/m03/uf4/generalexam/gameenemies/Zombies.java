package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.gameenemies;

public class Zombies extends Enemy {
    String so;

    public Zombies(String name, Integer life, String so) {
        super(name, life);
        this.so = so;
    }

    @Override
    public void attack(int fuerza) {
        if (life > 0) {
            life = life - fuerza;
            System.out.println(so);
        }
        if (life > 0) {
            System.out.printf("L'enemic %s té %d punts de vida després d'un atac de força %d\n", name, life, fuerza);
        } else System.out.printf("L'enemic %s ja està mort\n", name);
    }
}

