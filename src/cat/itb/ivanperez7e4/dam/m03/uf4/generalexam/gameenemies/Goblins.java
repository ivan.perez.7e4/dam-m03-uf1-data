package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.gameenemies;

public class Goblins extends Enemy{
    public Goblins(String name, Integer life) {
        super(name, life);
    }

    @Override
    public void attack(int fuerza) {
        if (fuerza<4){
            life=life-1;
        }
        else{
            life=life-5;
        }
        if (life>0){
            System.out.printf("L'enemic %s té %d punts de vida després d'un atac de força %d\n", name, life, fuerza);
        }
        else System.out.printf("L'enemic %s ja està mort\n", name);
    }
}
