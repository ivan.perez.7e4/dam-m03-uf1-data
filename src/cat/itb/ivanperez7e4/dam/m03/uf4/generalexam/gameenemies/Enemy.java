package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.gameenemies;

public abstract class Enemy {
    String name;
    Integer life;

    public Enemy(String name, Integer life) {
        this.name = name;
        this.life = life;
    }

    public abstract void attack(int fuerza);

    @Override
    public String toString() {
        return "Enemy{" +
                "name='" + name + '\'' +
                ", life=" + life +
                '}';
    }
}
