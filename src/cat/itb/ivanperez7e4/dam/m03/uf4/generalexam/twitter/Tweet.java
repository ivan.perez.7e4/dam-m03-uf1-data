package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.twitter;

public class Tweet {
    String user;
    String date;
    String text;

    public Tweet(String user, String date, String text) {
        this.user = user;
        this.date = date;
        this.text = text;
    }

    @Override
    public String toString() {
        return "@"+ user + " · "+ date+ "\n"
                + text +"\n\n";
    }
}
