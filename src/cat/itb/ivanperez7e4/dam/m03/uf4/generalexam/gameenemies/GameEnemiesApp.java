package cat.itb.ivanperez7e4.dam.m03.uf4.generalexam.gameenemies;

import java.util.ArrayList;
import java.util.List;

public class GameEnemiesApp {
    public static void main(String[] args) {
        List<Enemy> list = new ArrayList<>();
        list.add(new Zombies("Zog", 10, "AARRRrrgg"));
        list.add(new Zombies("Lili", 30, "GRAaaArg"));
        list.add(new Troll("Jum", 12, 5));
        list.add(new Goblins("Zog", 60));
        list.get(0).attack(5);
        list.get(0).attack(7);
        list.get(3).attack(7);
        list.get(1).attack(3);
        list.get(2).attack(4);
        list.get(2).attack(8);
        list.get(1).attack(4);
        list.get(0).attack(5);
        list.get(0).attack(1);
        list.get(2).attack(1);
        list.get(2).attack(1);
        list.get(2).attack(1);
        System.out.println(list);
    }
}
