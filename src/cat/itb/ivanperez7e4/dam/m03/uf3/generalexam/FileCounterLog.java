package cat.itb.ivanperez7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Scanner;

public class FileCounterLog {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        String date = LocalDateTime.now().toString();
        Path path = Paths.get(input, "counterlog.txt");


        try (OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            PrintStream printStream = new PrintStream(outputStream, true);
            printStream.println(date + " - Tens" + " fitxers a " + input);
        }
        //no sabia como leer la cantidad de archivos que hay en el path
        //creo que era con esta funcion pero a la hora de ponerlo en el print no me dejaba
    }
    //        private static void countArchivos(Path path) throws IOException {
//            Scanner scanner = new Scanner(path);
//            int num = 0;
//            while (scanner.hasNext()){
//                scanner.next();
//                num++;
//            }return num;
}
