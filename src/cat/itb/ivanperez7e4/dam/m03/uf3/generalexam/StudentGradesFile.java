package cat.itb.ivanperez7e4.dam.m03.uf3.generalexam;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.OldestStudent;
import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.StudentStats;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.util.Scanner;

public class StudentGradesFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();
        Path path = Paths.get(input);

        while(scanner.hasNext(String.valueOf(path))){
            String word = scanner.next();
            System.out.println(word);
        }
        //No se como hacerlo
    }
}
