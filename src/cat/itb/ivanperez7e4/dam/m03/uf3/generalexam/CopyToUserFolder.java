package cat.itb.ivanperez7e4.dam.m03.uf3.generalexam;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class CopyToUserFolder {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String path = scanner.nextLine();
        String nom = scanner.nextLine();
        String cognom = scanner.nextLine();
        String homePath = System.getProperty("user.home");
        Path bashrcPath = Paths.get(homePath, ".bashrc");

        Path folderBash = Paths.get(path,cognom,nom);
        Files.createDirectories(folderBash);
        Path destination = folderBash.resolve(".bashrc");

        Files.copy(bashrcPath, destination);
        System.out.printf("Fitxer copiat a la carpeta %s/%s/%s", path, cognom, nom);
    }
}
