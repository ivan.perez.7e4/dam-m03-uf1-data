package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

public class GenerateDummyFileStructure {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path folder = Paths.get(homePath, "dummyfolders");
        Files.createDirectories(folder);
        createSubFolders(folder);
    }

    private static void createSubFolders(Path folder) throws IOException {
        for(int i=1; i<=100; ++i){
            String name = i+"";
            Path subFolder = folder.resolve(name);
            Files.createDirectories(subFolder);
        }
    }
}

