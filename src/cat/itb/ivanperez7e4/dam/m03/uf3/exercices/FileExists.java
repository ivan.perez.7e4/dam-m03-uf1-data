package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Scanner;

public class FileExists {
    public static void main(String[] args) {
        Scanner scanner = new Scanner (System.in);
        String input = scanner.nextLine();
        Path path = Path.of(input);
        boolean exists = Files.exists(path);
        if (exists){
            System.out.println("True");
        }else {
            System.out.println("False");
        }
    }
}
