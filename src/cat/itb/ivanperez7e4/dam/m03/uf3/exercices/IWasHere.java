package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.time.LocalDateTime;
import java.util.Scanner;

public class IWasHere {
    public static void main(String[] args) throws IOException {
        String dateTime = LocalDateTime.now().toString();

        Path path = Paths.get("/home/sjo/Escriptori/i_was_here.txt");
        try (OutputStream outputStream = Files.newOutputStream(path, StandardOpenOption.APPEND, StandardOpenOption.CREATE)) {
            PrintStream printStream = new PrintStream(outputStream, true);
            printStream.println("I Was Here: " + dateTime);

        }
    }
}
