package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.LocalDateTime;

public class ProfileBackup {
    public static void main(String[] args) throws IOException {
        String homePath = System.getProperty("user.home");
        Path profilePath = Paths.get(homePath, ".profile");
        //~/backup/2021-02-16T09:59:04.343769/.profile
        String date = LocalDateTime.now().toString();
        Path backupFolder = Paths.get(homePath, "backup", date);
        Files.createDirectories(backupFolder);
        Path destination = backupFolder.resolve(".profile");
        Files.copy(profilePath, destination);


    }
}
