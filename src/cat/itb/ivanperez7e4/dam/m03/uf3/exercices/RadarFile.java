package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import cat.itb.ivanperez7e4.dam.m03.uf2.staticfunctions.IntegerLists;

import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;

public class RadarFile {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        List<Integer> speedList = readSpeedList(scanner);
        processSpeeds(speedList);
    }

    private static void processSpeeds(List<Integer> speedList) {
        int max = IntegerLists.max(speedList);
        int min  = IntegerLists.min(speedList);
        double average = IntegerLists.avg(speedList);

        System.out.printf("Velocitat màxima: %dkm/h%n", max);
        System.out.printf("Velocitat mínima: %dkm/h%n", min);
        System.out.printf("Velocitat mitjana: %.1fkm/h%n", average);
    }

    private static List<Integer> readSpeedList(Scanner scanner) throws IOException {
        String stringPath = scanner.nextLine();
        Path path = Paths.get(stringPath);
        return IntegerLists.readFromPath(path);
    }


}


