package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import java.util.InputMismatchException;
import java.util.Scanner;

public class WaitForInt {
    public static void main(String[] args) {
    Scanner scanner = new Scanner (System.in);
    try {
        int value = scanner.nextInt();
        System.out.printf("Número: %d", value);
        }
    catch(InputMismatchException exception){
        System.out.println("No és un enter");
        }
    }
}
