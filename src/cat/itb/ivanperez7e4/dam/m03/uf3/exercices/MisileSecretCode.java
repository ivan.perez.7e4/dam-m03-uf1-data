package cat.itb.ivanperez7e4.dam.m03.uf3.exercices;

import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Scanner;

public class MisileSecretCode {
    public static void main(String[] args) throws IOException {
        Scanner scanner = new Scanner(System.in);
        String input = scanner.nextLine();

        Path path = Paths.get("/home/sjo/Escriptori/secret.txt");
        Files.writeString(path, input);

        System.out.println("missil preparat");
    }
}
